<!doctype html>
<html lang="en" ng-app="railwayDashboard" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title>Pakistan Railway Dashboard</title>
    <link rel="stylesheet" href="bower_components/angular-material/angular-material.css"/>
    <link rel="stylesheet" href="style/custom.css"/>

    <link rel="stylesheet" href="http://js.arcgis.com/3.13/esri/css/esri.css">
   <link rel="stylesheet" type="text/css" href="bower_components/arcgis_js/agsjs/css/agsjs.css" />
    <link rel="stylesheet" type="text/css" href="bower_components/arcgis_js/arcgis_js_api/library/3.13/3.13/dijit/themes/claro/claro.css" />


    <link rel="stylesheet" type="text/css" href="bower_components/libs/bootstrap/css/bootstrap.min.css" />

    <link rel="stylesheet" type="text/css" href="bower_components/libs/bootstrap-table/dist/bootstrap-table.min.css" />

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/angular-material-data-table/0.10.8/md-data-table.min.css" />





<!--
  <link rel="stylesheet" type="text/css" href="//js.arcgis.com/3.9/js/dojo/dijit/themes/claro/claro.css">
  <link rel="stylesheet" type="text/css" href="//js.arcgis.com/3.9/js/esri/css/esri.css" />
 <link rel="stylesheet" type="text/css" href="assets/esri_api/src/agsjs/css/agsjs.css" />
-->
    <script type="text/javascript">
        var loadlandplan='<?php echo $_REQUEST['landplan'];?>'
     //   alert(loadlandplan);
        var dojoConfig = {
            paths: {
                agsjs: location.pathname.replace(/\/[^/]+$/, '') + '/assets/esri_api/agsjs/src/agsjs'
            }
        };
    </script>

   <!--<script src="http://js.arcgis.com/3.9/"></script>-->

    <style>
        .md-virtual-repeat-container{
            height: 195px;
        }

    </style>
</head>
<body class="claro" onload="init();">

<header>
    <div id="rwy_banner" layout="column" flex>
        <img src="images/Railway-Land-Banner-1.png" alt="Banner"/>
    </div>
</header>


<div id="rwy_combo" class="border-class ng-cloak" style="margin-bottom: 0px" ng-controller="filterController as filterCtrl">
<md-radio-group layout="row"  ng-model="rb_options"  ng-init="rb_options='railway'"  >
    <md-radio-button value="railway"  ng-click="switchGraphic()"  flex="10">Railway</md-radio-button>
    <md-radio-button value="revenue" ng-click="revStat()" flex="10">Revenue</md-radio-button>
    <md-radio-button value="mileage"  flex="10">Mileage</md-radio-button>


</md-radio-group>

<div layout="row" flex layout-padding style="margin-top:0px;padding-bottom: 0px"  >

    <!--<div layout="row" flex>-->

        <div layout="row"  md-theme="railwayColor" flex ng-show="rb_options == 'railway'">
            <md-input-container flex="15">
                <label>Division</label>
                <md-select ng-model="setDivisionVal.div">
                    <md-optgroup label="railway_division">
                        <md-option ng-repeat="division in filterCtrl.divisions" ng-value="{{division.uid}}" ng-click="filterCtrl.divisionSelect(division.id,division.extent,division.name)">{{ division.name }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="15">
                <label>Section</label>
                <md-select ng-model="setSectionVal.secid">
                    <md-optgroup label="railway_section">
                        <md-option ng-repeat="section in filterCtrl.sections" ng-value="{{section.id}}" ng-click="filterCtrl.sectionSelect(section.id,section.extent,section.name)">{{section.id}}--{{ section.name }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="15" ng-show="landplanBox">
                <label>Landplan</label>
                <md-select ng-model="setSheetVal.lpsheet">
                        <md-option ng-repeat="landplan in filterCtrl.landplans" ng-value="landplan.lp_sheet"  ng-click="filterCtrl.landplanSelect(landplan.lp_sheet,landplan.extent)">{{ landplan.lp_sheet }}</md-option>
                </md-select>
            </md-input-container>



            <md-input-container flex="15"  ng-show="lpchainageBox">
                <label>Landplan Chainage</label>
                <md-select ng-model="lp_chainage">
                    <md-optgroup label="lp_chainage">
                        <md-option ng-repeat="ch in filterCtrl.chainage" ng-value="{{ ch.id }}" ng-click="filterCtrl.chainageSelect(ch.ch_val,ch.symbology,ch.xmin,ch.ymin,ch.xmax,ch.ymax)">{{ ch.ch_val }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="15" ng-show="yardplanBox">
                <label>Yardplan</label>
                <md-select ng-model="railway_yardplan">
                    <md-optgroup label="railway_yardplan">
                        <md-option ng-repeat="yplp in filterCtrl.yardplanlp" ng-value="{{ yplp.tehsil_id }}" ng-click="filterCtrl.yardplanSelect(yplp.yp_sheet,yplp.moza)">{{ yplp.yp_sheet }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="15" ng-show="sidingBox">
                <label>Siding</label>
                <md-select ng-model="railway_siding">
                    <md-optgroup label="railway_siding">
                        <md-option ng-repeat="sid in filterCtrl.sidings" ng-value="{{ sid.lp_sheet }}" ng-click="filterCtrl.sidingSelect(sid.lp_sheet,sid.moza,sid.station,sid.extent)">{{ sid.lp_sheet }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="20" ng-show="parcelBox">
                <label>LandPlan Parcel</label>
                <md-select ng-model="setDropdownVal.pin">
                        <md-option ng-repeat="parcel in filterCtrl.parcels" ng-value="{{ parcel.gid }}" ng-click="filterCtrl.parcelSelect(parcel.old_pin,parcel.xmin,parcel.ymin,parcel.xmax,parcel.ymax,parcel.survey_typ,parcel.sub_parcel,parcel.unique_id,parcel.gid)">{{ parcel.old_pin }}</md-option>
                </md-select>
            </md-input-container>

            <md-input-container flex="15" ng-show="sidingparcelBox">
                <label>Siding Parcel</label>
                <md-select ng-model="siding_parcel">
                    <md-optgroup label="siding_parcel">
                        <md-option ng-repeat="sp in filterCtrl.sidingparcels" ng-value="{{ sp.id }}" ng-click="filterCtrl.sidingparcelSelect(sp.pin,sp.landuse,sp.moza,sp.station,sp.extent)">{{ sp.pin }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>



        </div>
        <div layout="row" flex ng-show="rb_options == 'revenue'">
            <md-input-container flex="20">
                <label>Province</label>
                <md-select ng-model="revenue_province">
                    <md-optgroup label="Province">
                        <md-option ng-repeat="pro in filterCtrl.province" ng-value="{{pro.id}}"  ng-click="filterCtrl.provinceSelect(pro.name,pro.id,pro.extent)">{{ pro.name }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>
            <md-input-container flex="20">
                <label>Division</label>
                <md-select ng-model="revenue_division">
                    <md-optgroup label="divisions">
                        <md-option ng-repeat="div in filterCtrl.rev_divisions" ng-value="{{div.id}}" ng-click="filterCtrl.revnueDivisionSelect(div.name,div.id,div.extent)">{{ div.name }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>
            <md-input-container flex="20">
                <label>District</label>
                <md-select ng-model="revenue_district">
                    <md-optgroup label="district">
                        <md-option ng-repeat="dis in filterCtrl.rev_district" ng-value="{{dis.id}}" ng-click="filterCtrl.revnueDistrictSelect(dis.name,dis.id,dis.extent)" >{{ dis.name }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>
            <md-input-container flex="20">
                <label>Tehsil</label>
                <md-select ng-model="revenue_tehsil">
                    <md-optgroup label="tehsil">
                        <md-option ng-repeat="teh in filterCtrl.rev_tehsil" ng-value="{{teh.id}}" ng-click="filterCtrl.revnueTehsilSelect(teh.name,teh.id,teh.extent)" >{{ teh.name }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>
            <md-input-container flex="20">
                <label>Mauza</label>
                <md-select ng-model="revenue_moza">
                    <md-optgroup label="moza">
                        <md-option ng-repeat="moza in filterCtrl.rev_moza" ng-value="{{moza.id}}" ng-click="filterCtrl.revnueMozaSelect(moza.name,moza.id,moza.extent)" >{{ moza.name }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="20">
                <label>Landplan</label>
                <md-select ng-model="revenue_landplan">
                    <md-optgroup label="landplan">
                        <md-option ng-repeat="rev_lp in filterCtrl.rev_landplan" ng-value="{{rev_lp.lp_sheet}}" ng-click="filterCtrl.landplanSelect(rev_lp.lp_sheet,rev_lp.extent)">{{ rev_lp.lp_sheet }}</md-option>

                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="15">
                <label>Landplan Chainage</label>
                <md-select ng-model="rev_chainage">
                    <md-optgroup label="rev_chainage">
                        <md-option ng-repeat="ch in filterCtrl.chainage" ng-value="{{ ch.id }}" ng-click="filterCtrl.chainageSelect(ch.ch_val,ch.symbology,ch.xmin,ch.ymin,ch.xmax,ch.ymax)">{{ ch.ch_val }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="20">
                <label>Parcel</label>
                <md-select ng-model="revenue_parcel">
                    <md-optgroup label="parcel">
                        <md-option ng-repeat="parcel in filterCtrl.parcels" ng-value="{{ parcel.gid }}" ng-click="filterCtrl.parcelSelect(parcel.old_pin,parcel.xmin,parcel.ymin,parcel.xmax,parcel.ymax,parcel.survey_typ,parcel.sub_parcel,parcel.unique_id)">{{ parcel.old_pin }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

        </div>
        <div layout="row" flex ng-show="rb_options=='mileage'">

            <md-input-container flex="15">
                <label>Division</label>
                <md-select ng-model="railway_division">
                    <md-optgroup label="railway_division">
                        <md-option ng-repeat="division in filterCtrl.divisions" ng-value="{{division.uid}}" ng-click="filterCtrl.divisionSelect(division.id,division.extent,division.name)">{{ division.name }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>


            <md-input-container flex="20">
                <label>Mileage From</label>
                <md-select ng-model="mileage_from">
                    <md-optgroup label="mileage from">
                        <md-option ng-repeat="mile in filterCtrl.mileageFrom" ng-value="{{mile.mileage}}" >{{ mile.mileage }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="20">
                <label>Mileage To</label>
                <md-select ng-model="mileage_to">
                    <md-optgroup label="mileage to">
                        <md-option ng-repeat="mile in filterCtrl.mileageTo" ng-click="selectLandplanFromMileage()" ng-value="{{mile.mileage}}" >{{ mile.mileage }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>

            <md-input-container flex="15">
                <label>Landplan</label>
                <md-select ng-model="railway_landplan">
                    <md-optgroup label="railway_landplan">
                        <md-option ng-repeat="landplan in filterCtrl.landplans" ng-value="{{ landplan.lp_sheet }}" ng-click="filterCtrl.landplanSelect(landplan.lp_sheet,landplan.extent)">{{ landplan.lp_sheet }}</md-option>
                    </md-optgroup>
                </md-select>
            </md-input-container>



        </div>
    <!--</div>-->
</div>
    </div>

<div layout="row">

    <div flex="20"  class="panels_height " layout-margin  >
     <md-card style="background-color:#3498DB;margin: 0 0 0 0"><h1 style="font-size:15px" >TOC</h1></md-card>
        <div id="legendDiv" style="height: 90%" class="scrollbar"></div>



    </div>


    <div flex id="map" class="panels_height" layout-margin ng-controller="filterController as filterCtrl">
        <div class="toolbar" style="width: 1044px; height:30px;background-color:#3498DB;zoom:1;margin: 0 0 0 0;background: -moz-linear-gradient(top,  #3498DB 0%, #3498DB 32%, #3498DB 72%, #3498DB 84%,
         #3498DB 90%, #3498DB 100%);background: -webkit-linear-gradient(top,  #3498DB 0%,#3498DB 32%,#3498DB 72%,#3498DB 84%,#3498DB 90%,#3498DB 100%);
">
            <button><img src="images/nav_zoomin.png" onclick="navigationToolBar('zoomin')" title="Zoom in"></button>
            <button><img src="images/nav_zoomout.png" onclick="navigationToolBar('zoomout')" title="Zoom out"></button>
            <!--
            <button><img src="images/i_info.png" title="Information"></button>
            -->
            <button><img src="images/nav_pan.png" onclick="navigationToolBar('pan')" title="Pan"></button>
            <!--
            <button><img src="images/Measure_Area16.png" title="Measure Area"></button>
            <button><img src="images/Measure_Point16.png" title="Measure Point"></button>
            -->
            <button><img src="images/ZoomFullExtent.png" onclick="navigationToolBar('zoomtofullextent')" title="Zoom to Full Extent"></button>
            <button><img src="images/nav_previous.png" onclick="navigationToolBar('pre')" title="Previous Extent"></button>
            <button><img src="images/nav_next.png" onclick="navigationToolBar('next')" title="Next Extent"></button>
            <button><img src="images/i_info.png" onclick="navigationToolBar('identify')" title="Information"></button>
            <button><img src="images/nav_decline.png" onclick="navigationToolBar('deactive')"  title="Deactivate"></button>
            <button><img src="images/arrow_refresh.png" ng-click="switchGraphic()" title="Refresh"></button>
            <!--
            <button><img src="images/Clear.png" title="Clear Selection"></button>
            <button><img src="images/arrow_refresh.png" title="Refresh"></button>
            <button><img src="images/database-icon.png" title="Database"></button>
            -->
        </div>

    </div>

    <!--right panel-->
    <div flex="20" id="infodivs"  class="panels_height" layout-margin ng-controller="filterController as filterCtrl" >
        <md-card style="background-color:#3498DB;margin: 0 0 0 0;"><h1 id="infoheading" style="font-size:15px">Info</h1></md-card>

        <!--
           <mdt-table virtual-repeat="true">
               <mdt-header-row>
                   <mdt-column align-rule="left">Dessert (100g serving)</mdt-column>
                   <mdt-column align-rule="right">Calories</mdt-column>
                   <mdt-column align-rule="right">Fat (g)</mdt-column>

               </mdt-header-row>
               <mdt-row ng-repeat="division in filterCtrl.divisions">
                   <mdt-cell>{{division.uid}}</mdt-cell>
                   <mdt-cell>{{division.id}}</mdt-cell>
                   <mdt-cell>{{division.name}}</mdt-cell>


               </mdt-row>
           </mdt-table>
         -->
        <div style=" height:90%" id="tbldataview" class="scrollbar" >
        <table class="table table-striped" style="margin: 0 0 0 0">
            <!--
            <thead>
            <tr>
                <th>uid</th>
                <th>section id</th>
                <th>name</th>
            </tr>
            </thead>
            <tbody ng-repeat="division in filterCtrl.divisions">
            <tr>
                <td>{{division.uid}}</td>
                <td>{{division.id}}</td>
                <td>{{division.name}}</td>
            </tr>

            </tbody>
            -->
        </table>
        </div>



    </div>



</div>


<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Area Comparison</h4>
            </div>
            <div class="modal-body">
                <div id="container" style="min-width: 550px; height: 400px; margin: 0 auto"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

</div>





<script type="text/javascript" src="bower_components/libs/jquery/jquery.min.js"></script>



<!-- Start bootstrap table -->
<script type="text/javascript" src="bower_components/libs/bootstrap/js/bootstrap.min.js"></script>

<script type="text/javascript" src="bower_components/libs/bootstrap-table/dist/bootstrap-table.min.js"></script>
<!-- for column position change -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>


<script type="text/javascript" src="bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="bower_components/angular-material/angular-material.min.js"></script>
<script type="text/javascript" src="bower_components/angular-aria/angular-aria.min.js"></script>
<script type="text/javascript" src="bower_components/angular-animate/angular-animate.min.js"></script>
<script type="text/javascript" src="bower_components/angular-messages/angular-messages.min.js"></script>

<script type="text/javascript" src="bower_components/highcharts/highcharts.js"></script>
<script type="text/javascript" src="bower_components/highcharts/modules/exporting.js"></script>

<script src="bower_components/arcgis_js/arcgis_js_api/library/3.13/3.13/init.js"></script>


<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular-material-data-table/0.10.8/md-data-table.min.js"></script>
<script type="text/javascript" src="script/main.js"></script>
<script type="text/javascript" src="script/controllers/selection.controller.js"></script>

<script type="text/javascript" src="script/source/jquery.fancybox.js?v=2.1.5"></script>
<link rel="stylesheet" type="text/css" href="script/source/jquery.fancybox.css?v=2.1.5" media="screen" />

<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="script/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" />
<script type="text/javascript" src="script/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>

<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css" href="script/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" />
<script type="text/javascript" src="script/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>

<!-- Add Media helper (this is optional) -->
<script type="text/javascript" src="script/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>




<script>
$(document).ready(function () {
$(".fancybox-button").fancybox({
openEffect	: 'none',
closeEffect	: 'none',
    fitToView: false,
    beforeShow: function () {
        // apply new size to img
        $(".fancybox-button-image").css({
            "width": 1000,
            "height": 600
        });
        // set new values for parent container
        this.width = 1000;
        this.height = 600;
    },
    prevEffect		: 'none',
    nextEffect		: 'none',
    closeBtn		: false,
    helpers		: {
        title	: { type : 'inside' },
        buttons	: {}
    }
});
    $( window ).resize(function() {
        var windowHeight = $(window).height();
        h=$("#rwy_banner").outerHeight(true)+$("#rwy_combo").outerHeight(true)+20;
        $('.panels_height').css('height',windowHeight-h+'px');
    });
    h=$("#rwy_banner").outerHeight(true)+$("#rwy_combo").outerHeight(true)+20;
  //  alert(h);
    var windowHeight = $(window).height();
    $('.panels_height').css('height',windowHeight-h+'px');
});
</script>



</body>
</html>
angular.module('railwayDashboard', ['ngMaterial','selectionController'])
    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('railwayColor')
            .primaryPalette('green')
            .accentPalette('light-green');
    });



var map;
var dynLayersList;
var navToolbar;
var initExtent;
function init() {


     dynLayersList= {
        //"Musavi":"http://202.166.167.119:6080/arcgis/rest/services/Railways/musavees/MapServer/",
        "Land Plan":"http://202.166.167.125:6080/arcgis/rest/services/Pakistan/Pakistan_railway_landplan_r_30092016/MapServer/",
        //"Yard Plan":"http://202.166.167.119:6080/arcgis/rest/services/Railways/yardplans/MapServer/",
        //"Railway Vector Data":"http://202.166.167.119:6080/arcgis/rest/services/Railways/PAKISTAN_RAILWAYS_1/MapServer/"
        "Musavi":"http://202.166.167.125:6080/arcgis/rest/services/Pakistan/Pakistan_railway_musave_r_30092016/MapServer/",
        "Yard Plan":"http://202.166.167.125:6080/arcgis/rest/services/Railway/yardplan/MapServer/",
      //  "Railway Vector Data":"http://202.166.167.125:6080/arcgis/rest/services/Railway/PR_Vector/MapServer/"
         "Railway Vector Data":"http://202.166.167.125:6080/arcgis/rest/services/Railway/PAKISTAN_RAILWAYS31082016/MapServer/"

    };
    var dynLayers= [];

  //  dojo.require("esri.map");
  //  dojo.require("esri.tasks.query");
  //  loading = dojo.byId("loadingImg");
    require(["dojo/_base/connect",
        "dojo/dom", "dojo/parser", "dojo/on", "dojo/_base/Color",
        "esri/map",
        "esri/geometry/Extent",
        "esri/layers/FeatureLayer",
        "esri/layers/ArcGISTiledMapServiceLayer",
        "esri/layers/ArcGISDynamicMapServiceLayer",
        "esri/symbols/SimpleFillSymbol",
        "esri/renderers/ClassBreaksRenderer",
        "agsjs/dijit/TOC",
      //  "agsjs/layers/GoogleMapsLayer",
     //   "dijit/layout/BorderContainer",
      //  "dijit/layout/ContentPane",
        "esri/layers/ImageParameters",
        "esri/toolbars/navigation",
        "esri/tasks/IdentifyTask",
        "esri/tasks/IdentifyParameters",
        "dojo/_base/array",
       // "dojo/on",
        "dijit/registry",
        //"dijit/Toolbar",
       // "dijit/form/Button",
      //  "dojo/fx", "dojo/domReady!"
        ],
        function (connect, dom, parser, on, Color, Map, Extent, FeatureLayer, ArcGISTiledMapServiceLayer, ArcGISDynamicMapServiceLayer, SimpleFillSymbol, ClassBreaksRenderer, TOC, ImageParameters,Navigation,IdentifyTask,IdentifyParameters,arrayUtils, registry) {

        initExtent = new esri.geometry.Extent({
            "xmin": 59.502226899466805,
            "ymin": 23.667462523000033,
            "xmax": 76.70448042253331,
            "ymax": 35.617430355000074,
            "spatialReference": {
                "wkid": 4326
            }
        });
        map = new Map("map", {
            extent: initExtent,
            maxZoom: 19,
            logo: false
        });

            esriConfig.defaults.map.zoomDuration = 0;
            esriConfig.defaults.map.zoomRate = 0;
        parser.parse();

        //Add Basemap
        //var basemap = new ArcGISTiledMapServiceLayer("http://server.arcgisonline.com/arcgis/rest/services/World_Imagery/MapServer");
        //map.addLayer(basemap);

            var railwayImage1 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_1/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage1);

            var railwayImage2 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_2/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage2);

            var railwayImage3 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_3/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage3);

            var railwayImage4 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_4/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage4);

            var railwayImage5 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_5/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage5);

            var railwayImage6 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_6/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage6);

            var railwayImage7 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_7/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage7);

            var railwayImage8 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_8/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage8);

            var railwayImage9 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_9/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage9);

            var railwayImage10 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_10/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage10);

            var railwayImage11 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_11/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage11);

            var railwayImage12 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_12/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage12);

            var railwayImage13 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_13/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage13);

            var railwayImage14 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_14/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage14);

            var railwayImage15 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_15/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage15);

            var railwayImage16 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_16/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage16);

            var railwayImage17 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_17/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage17);

            var railwayImage18 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_18/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage18);

            var railwayImage19 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_19/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage19);

            var railwayImage20 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_20/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage20);

            var railwayImage21 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_21/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage21);

            var railwayImage22 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_22/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage22);

            var railwayImage23 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_23/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage23);

            var railwayImage24 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_24/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage24);

            var railwayImage25 = new ArcGISDynamicMapServiceLayer("http://202.166.167.125:6080/arcgis/rest/services/Pakistan_RailwayImagery/PKR_Imagery_25/MapServer/", {
                opacity: 1,
                visible: true
            });
            map.addLayer(railwayImage25);

        for (var key in dynLayersList) {
            dynLayers.push(new ArcGISDynamicMapServiceLayer(dynLayersList[key], {
                opacity: 1
            }));
        }
        map.addLayers(dynLayers);




        map.on('layers-add-result', function (evt) {
            //alert("Evt:"+JSON.parse(evt));
            var layerInfos = [];
            var i = 0;
            for (var key in dynLayersList) {
                var obj = {
                    layer: dynLayers[i++],
                    title: key,
                    slider: true,
                    collapsed: true, // whether this root layer should be collapsed initially, default false.
                    //slider: false // whether to display a transparency slider.
                    autoToggle:false
                }
                layerInfos.push(obj);

            }
            var toc = new TOC({
                map:map,
                layerInfos: layerInfos
            }, 'legendDiv');

            toc.startup();

        });

        //Add Google layer
        //var googleLayer = new agsjs.layers.GoogleMapsLayer({
        //    id: 'google',
        //    apiOptions: {
        //        v: '3.6'
        //    },
        //    mapOptions: {
        //        streetViewControl: false
        //    }
        //});
        //
        //googleLayer.setMapTypeId(agsjs.layers.GoogleMapsLayer.MAP_TYPE_HYBRID);
        //map.addLayer(googleLayer);

            navToolbar = new Navigation(map);

    });

}


function zoomToExtent(extent){
    var arrExtent = extent.split(',');

       map.setExtent(new esri.geometry.Extent({
           "xmin": parseFloat(arrExtent[0]),
           "ymin": parseFloat(arrExtent[1]),
           "xmax": parseFloat(arrExtent[2]),
           "ymax": parseFloat(arrExtent[3]),
           "spatialReference": {"wkid": 4326}
       }), true);

    //  me.map.setLevel(me.map.getLevel()-1);
}

   function navigationToolBar(tool){
       require([
               "esri/toolbars/navigation",
               "esri/tasks/IdentifyTask",
               "esri/tasks/IdentifyParameters",
               "dojo/_base/array"
           ],
           function (Navigation,IdentifyTask,IdentifyParameters,arrayUtils) {


               if(tool=='zoomin'){
                   //navToolbar.deactivate();
                   //map.disablePan();

                   navToolbar.activate(Navigation.ZOOM_IN);
               }
               else if(tool=='zoomout'){
                   //navToolbar.deactivate();
                   //map.disablePan();

                   navToolbar.activate(Navigation.ZOOM_OUT);
               }else if(tool=='zoomtofullextent'){
                   var location = [73, 31];
                   map.centerAndZoom(location, 6);
               }else if(tool=='pan'){

                   navToolbar.activate(Navigation.PAN);
               }else if(tool=='pre'){
                   //navToolbar.deactivate();
                   //map.disablePan();


                   navToolbar.zoomToPrevExtent();
               }else if(tool=='next'){
                   //navToolbar.deactivate();
                   //map.disablePan();


                   navToolbar.zoomToNextExtent();
               }else if(tool=='deactive'){
                   //navToolbar.deactivate();
                   //map.disablePan();

                   navToolbar.deactivate();

               }else if(tool=='identify'){
                   map.on("click", executeIdentifyTask);
                   identifyTask = new IdentifyTask("http://202.166.167.125:6080/arcgis/rest/services/Railway/PAKISTAN_RAILWAYS31082016/MapServer/");
                   identifyParams = new IdentifyParameters();
                   identifyParams.tolerance = 3;
                   identifyParams.layerIds = [7,9,18,1];
                   identifyParams.returnGeometry = true;
                   identifyParams.layerOption = IdentifyParameters.LAYER_OPTION_ALL;
                   identifyParams.width = map.width;
                   identifyParams.height = map.height;




           }

               function executeIdentifyTask(evt) {
                   map.graphics.clear();
                   identifyParams.geometry = evt.mapPoint;
                   identifyParams.mapExtent = map.extent;
                   var deferred = identifyTask.execute(identifyParams);
                   deferred.addCallback(function (response) {
                       return arrayUtils.map(response, function (result) {
                           var feature = result.feature;
                           feature.attributes.layerName = result.layerName;

                           if(result.layerName=='Satellite Landuse Parcels'){
                               var scp = angular.element($('#rwy_combo')).scope();
                               scp.autoFillDropDown(feature.attributes.old_pin,feature.attributes.survey_typ,feature.attributes.sub_parcel,feature.attributes.gid);
                           }

                           var myTable = "<table width='250' border='2' style='border-collapse:collapse; border-color:#000000; border:solid'> ";
                           for (var key in feature.attributes) {

                               myTable += "<tr><td>" + key + "</td><td>" + feature.attributes[key] + "</td></tr>";
                           }
                           myTable += "</table>";
                           var template = new esri.InfoTemplate("Information", "" +
                           "<div align='center' id='div' >" + myTable +
                           "</div>");
                           feature.setInfoTemplate(template);
                           return feature;
                       });
                   });
                   map.infoWindow.setFeatures([deferred]);
                   map.infoWindow.show(evt.mapPoint);
               }


           });

}






function selectFeature(dynLayerName, layerIndex, colName, colVal,symbology) {
    //alert("LayerName: "+dynLayerName+", Layer index :"+layerIndex+", colNam: "+colName+", colVal: "+colVal);
   // var me = this;
    require([
        "esri/tasks/QueryTask",
        "esri/tasks/query", "esri/Color",
        "esri/graphic",
        "esri/symbols/PictureMarkerSymbol",
        "esri/symbols/SimpleFillSymbol",
        "esri/symbols/SimpleLineSymbol",
        "esri/symbols/TextSymbol",
        "esri/layers/LabelClass",


    ], function (QueryTask, Query, Color, Graphic, PictureMarkerSymbol,
                 SimpleFillSymbol, SimpleLineSymbol,TextSymbol,LabelClass) {

        var queryTask = new QueryTask(dynLayersList[dynLayerName] + layerIndex);
        //alert(colName +'='+ colVal);
        var selectQuery = new Query();
        selectQuery.where = colName + " = '" + colVal + "'";
        selectQuery.returnGeometry = true;
        selectQuery.outFields = [""];
        selectQuery.outSpatialReference = {wkid:4326};
        queryTask.execute(selectQuery, function (featureSet) {
            //remove all graphics on the maps graphics layer

            map.graphics.clear();

            //Performance enhancer - assign featureSet array to a single variable.
            var resultFeatures = featureSet.features;

            //Loop through each feature returned
            for (var i = 0, il = resultFeatures.length; i < il; i++) {
                //Get the current feature from the featureSet.
                //Feature is a graphic
                var graphic = resultFeatures[i];
                var symbol
                if (graphic.geometry.type == 'point') {
                    symbol = new PictureMarkerSymbol("images/flashmarker.gif", 15, 15);
                    //var marker1 = new Graphic({
                    //    "geometry": {
                    //        "x": graphic.attributes.e, "y": graphic.attributes.n,
                    //        "spatialReference": {"wkid": 4326}
                    //    }, "attributes": {}
                    //});

                    //var statesColor = new Color("#666");
                    //var chainageLable = new TextSymbol().setColor(statesColor);
                    //chainageLable.font.setSize("14pt");
                    //chainageLable.font.setFamily("arial");
                    //var json = {
                    //    "labelExpressionInfo": {"value":symbology}
                    //};

                    //var labelClass = new LabelClass(json);
                    //labelClass.symbol = chainageLable; // symbol also can be set in LabelClass' json
                    //symbol.setLabelingInfo([ labelClass ]);

                    var font  = new esri.symbol.Font();
                    font.setSize("12pt");
                    font.setWeight(esri.symbol.Font.WEIGHT_BOLD);

                    var schTextSymbol = new esri.symbol.TextSymbol(symbology);
                    schTextSymbol.setFont(font);
                    schTextSymbol.setColor(new dojo.Color([255, 0, 0]));
                    schTextSymbol.setKerning(true);
                    schTextSymbol.setAlign(esri.symbol.TextSymbol.ALIGN_START);
                    schTextSymbol.setAngle(45);

                    var pt;

                    pt = graphic.geometry;
                    var gra = new esri.Graphic(pt, schTextSymbol);
                    map.graphics.add(gra);

                    graphic.setSymbol(symbol);
                    map.graphics.add(graphic);
                    //showSchemeAttributes(resultFeatures[i].attributes)
                } else {
                    symbol = new SimpleFillSymbol(
                        SimpleFillSymbol.STYLE_NULL,
                        new SimpleLineSymbol(
                            SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0, 255, 255]), 4), new dojo.Color([255, 255, 0, 0.5]));
                    graphic.setSymbol(symbol);
                    map.graphics.add(graphic);
                }


                //Set the infoTemplate.
//                graphic.setInfoTemplate(infoTemplate);

                //Add graphic to the map graphics layer.


            }
        });
    });
}


//function createGrid(url) {
//    $('#rightPanel').bootstrapTable({
//        url:url,
//        striped: true,
//      //  toolbar: "#member-filter",
//        sidePagination: "client",
//        search: "true",
//        showRefresh: "true",
//        showToggle: "true",
//        showColumns: "true",
//        showExport: "true",
//        sortable: "true",
//        sortName: "id",
//        sortOrder: "desc",
//        minimumCountColumns: "2",
//        showPaginationSwitch: "true",
//        pagination: "true",
//        idField: "id",
//        pageList: "[10, 25, 50, 100, ALL]",
//        showFooter: "false",
//        reorderableColumns: "true",
//        filterControl: "true",
//        //resizable : "true"
//        onCheck: function (row, element) {
//            var selRows = getIdSelections();
//            setButtonVisibility(selRows.length);
//        },
//        onUncheck: function (row, element) {
//            var selRows = getIdSelections();
//            setButtonVisibility(selRows.length);
//        },
//        onCheckAll: function (row, element) {
//            var selRows = getIdSelections();
//            setButtonVisibility(selRows.length);
//        },
//        onUncheckAll: function (row, element) {
//            var selRows = getIdSelections();
//            setButtonVisibility(selRows.length);
//        },
//        onClickRow: function (row, element) {
//            //alert("working");
//         //   window.location.assign("/en/shopDetail?shopid=" + row.id);
//        }
//    });
//
//}

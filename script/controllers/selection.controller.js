var globalData;
var rwyDiv;
var rwyDivId;
var rwySec;
var appController=angular.module('selectionController',[])
.controller('filterController', [ '$scope', '$http', '$filter','$timeout', function($scope,$http,$filter,$timeout){

        var list = this;
        list.province=[];
        list.divisions = [];
        list.rev_district = [];
        list.rev_tehsil = [];
        list.rev_moza = [];
        list.rev_divisions = [];
        list.rev_landplan=[];
        list.sections = [];
        list.landplans = [];
        list.chainage=[];
        list.landplans1 = [];
        list.yardplans = [];
        list.sidings = [];
        list.sidingparcels = [];
        list.parcels = [];
        list.parcelInfo = [];
        list.attributes = [];
        list.secAttributes=[];
        list.pictures=[];
        list.pictures1=[];
        list.yardplanlp=[];
        list.mileageFrom =[];
        list.mileageTo = [];
        $scope.landplanBox=false;
        $scope.yardplanBox=false;
        $scope.sidingBox=false;
        $scope.sidingparcelBox=false;
        $scope.parcelBox=false;
        $scope.lpchainageBox=false;
        $scope.setDropdownVal={};
        $scope.setSheetVal={}
        $scope.setDivisionVal={}
        $scope.setSectionVal={}


        var main=this;






        $http({
            method: 'GET',
            url: 'services/GetComboesData.php?ADMIN_LEVEL=rdivision&LEVEL_ID=0'
        }).success(function(data){
            list.divisions = data;


        });

        $http({
            method: 'GET',
            url: 'services/GetComboesData.php?ADMIN_LEVEL=province&LEVEL_ID=0'
        }).success(function(data){
            list.province = data;
            // alert(data);
        });

  if(loadlandplan!="") {
      setTimeout(function () {
          $http({
              method: 'GET',
              url: 'services/rdivisionDetail.php?levelid=startload&lp=' + loadlandplan
          }).success(function (data) {
              $scope.landplanBox = true;
              $scope.landplanSelect1(loadlandplan, data[0].extent)
              var divi=list.divisions;
              var seci = null;
              for(var i=0;i<divi.length;i++){
                  if(data[0].uid==divi[i].uid){
                      main.divisionSelect(divi[i].id,'',divi[i].name);
                      seci = list.sections;
                  }
              }
              $scope.setDivisionVal={
                  div:data[0].uid
              };


              var secStr=data[0].id.split("-");
              $scope.setSectionVal = {
                  secid:secStr[0]
              };



              for(var i=0;i<seci.length;i++) {
                  if(data[0].id==seci[i].id){
                      main.sectionSelect(seci[i].id,'',seci[i].name);
                      $scope.setSheetVal= {
                          lpsheet:loadlandplan
                      }
                  }
              }



          });
      }, 1000);
  }
        $http({
            method: 'GET',
            url: 'services/rdivisionDetail.php?levelid=totalStats'
        }).success(function(data) {
               $('#infoheading').html("Railway Statistics");
               $("#tbldataview").html();
                var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                    '<tbody>';
                for(var i=0;i<data.length;i++) {
                    str = str + '<tr>' +
                    '<th>Total Division</th>' +
                    '<td>'+data[i].totaldivision+'</td>'+
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Sections</th>'+
                    '<td>'+data[i].totalsection+'</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<th>Total LandPlans</th>'+
                    '<td>'+data[i].totallandplans+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Yardplans</th>'+
                    '<td>'+data[i].totalyardplans+'</td>' +
                    '</tr>'
                    //'<tr>' +
                    //'<th>Total Sidings</th>'+
                    //'<td>'+data[i].totalsidings+'</td>' +
                    //'</tr>'



                }
                str=str+'</tbody>'+
                '</table>';
                if(loadlandplan=="") {
                     $("#tbldataview").html(str);
                }
            });


        this.divisionSelect = function(division,extent,name){
              if(extent!='') {
                  zoomToExtent(extent);
                  selectFeature("Railway Vector Data", 17, "id", division);
                  $scope.selectMileage(division);
                  list.attributtesSelect(division);
                  rwyDiv = name;
                  rwyDivId = division;
              }
            var data = $.param({
                division_id : division
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            //$http.post('services/GetComboesData.php?ADMIN_LEVEL=section&LEVEL_ID='+division, data, config)
            //    .success(function(data){
            //        list.sections = data;
            //
            //    });
            $.ajax({
                type: "GET",
                url: 'services/GetComboesData.php?ADMIN_LEVEL=section&LEVEL_ID='+division,
                async: false,
                success : function(data) {
                    var data1=eval('('+data+')');
                    list.sections = data1;
                }
            });

        }

        this.provinceSelect = function(province,pro_id,extent){

            zoomToExtent(extent);
           selectFeature("Railway Vector Data", 13, "name", province);

            //    createGrid('services/rdivisionDetail.php');

            var data = $.param({
                division_id : province
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=division&LEVEL_ID='+pro_id, data, config)
                .success(function(data){
                    list.rev_divisions = data;
                });

            $http({
                method: 'GET',
                url: 'services/rdivisionDetail.php?levelid=pro_stat&pro_id='+pro_id
            }).success(function(data) {
                $('#infoheading').html("Revenue Statistics");
                $("#tbldataview").html();
                var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                    '<tbody>';
                for(var i=0;i<data.length;i++) {
                    str = str + '<tr>' +
                    '<th>Total Division</th>' +
                    '<td>'+data[i].div_count+'</td>'+
                    '</tr>'+
                    '<tr>' +
                    '<th>Total District</th>'+
                    '<td>'+data[i].dist_count+'</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<th>Total Tehsil</th>'+
                    '<td>'+data[i].teh_count+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Moza</th>'+
                    '<td>'+data[i].moza_count+'</td>' +
                    '</tr>';

                }
                str=str+'</tbody>'+
                '</table>';

                $("#tbldataview").html(str);

            });


        }

        this.revnueDivisionSelect = function(div,id,extent){

            zoomToExtent(extent);
            selectFeature("Railway Vector Data", 14, "name", div);

            //    createGrid('services/rdivisionDetail.php');

            var data = $.param({
                division_id : id
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=district&LEVEL_ID='+id, data, config)
                .success(function(data){
                    list.rev_district = data;
                });
        }

        this.revnueDistrictSelect=function(dis,id,extent){
            zoomToExtent(extent);
            selectFeature("Railway Vector Data", 15, "name", dis);

            //    createGrid('services/rdivisionDetail.php');

            var data = $.param({
                division_id : id
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=tehsil&LEVEL_ID='+id, data, config)
                .success(function(data){
                    list.rev_tehsil = data;
                });

        }

        this.revnueTehsilSelect=function(teh,id,extent){
            zoomToExtent(extent);
            selectFeature("Railway Vector Data", 16, "name", teh);

            //    createGrid('services/rdivisionDetail.php');

            var data = $.param({
                division_id : id
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=moza&LEVEL_ID='+id, data, config)
                .success(function(data){
                    list.rev_moza= data;
                });

        }

        this.revnueMozaSelect=function(moza,id,extent){
            zoomToExtent(extent);
            selectFeature("Railway Vector Data", 19, "name", moza);

            var data = $.param({
                division_id : id
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=mozalandplan&LEVEL_ID='+moza, data, config)
                .success(function(data){
                    list.rev_landplan= data;
                });




        }


        this.attributtesSelect = function(division){

            var data = $.param({
                division_id : division
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/rdivisionDetail.php?divid='+division+'&levelid=rdivision', data, config)
                .success(function(data){
                    list.attributes = data;
                    globalData=data;
                    $('#infoheading').html("Division Info");

                    $scope.landplanBox = false
                    $scope.yardplanBox = false
                    $scope.sidingBox = false
                    $scope.sidingparcelBox = false
                    $scope.parcelBox = false
                    $scope.lpchainageBox = false


                    $("#tbldataview").html();
                    var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                        '<tbody>';
                        for(var i=0;i<data.length;i++) {
                            str = str + '<tr>' +
                            '<th>Division</th>' +
                            '<td>'+data[i].id+'</td>'+
                            '</tr>'+
                            '<tr>' +
                            '<th>Total Sections</th>'+
                            '<td>'+data[i].sec_count+'</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Total Moza</th>'+
                            '<td>'+data[i].moza_count+'</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Total Landplan</th>'+
                            '<td>'+data[i].lp_count+'</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Total Parcel</th>'+
                            '<td>'+data[i].parcel_count+'</td>' +
                            '</tr>'+
                            //'<tr>' +
                            //'<th>Total Siding</th>'+
                            //'<td>'+data[i].siding+'</td>' +
                            //'</tr>'+
                            '<tr>' +
                            '<th>Total Yardplan</th>'+
                            '<td>'+data[i].yardplan+'</td>' +
                            '</tr>'+
                            '<tr class=danger>' +
                            '<th colspan="2" style="text-align: center"> Area Info  <select onchange="unitConversion(value)"><option>Sq Yrd</option><option>Sq Feet</option><option>Acres</option><option>Kanals</option><option>Marlas</option></select> </th>'+
                            '</tr>'+
                            '<tr class=danger>' +
                            '<th>Landplan</th>'+
                            '<td id="lp">'+commaSeparateNumber(data[i].lp_area)+'</td>' +
                            '</tr>'+
                            '<tr class=danger>' +
                            '<th>ROR Permanent</th>'+
                            '<td id="mh">'+ commaSeparateNumber(data[i].mh)+'</td>' +
                            '</tr>'+
                            '<tr class=danger>' +
                            '<th>ROR Periodic</th>'+
                            '<td id="rh">'+commaSeparateNumber(data[i].rh)+'</td>' +
                            '</tr>'+
                            '<tr>'+
                            '<th>Area Comparison Chart</th>'+
                            '<td>  <a href=javascript:areaComparisionChart('+data[i].lp_area+','+data[i].mh+','+data[i].rh+')>View Chart</a></td>'+
                            '</tr>';
                        }
                    str=str+'</tbody>'+
                        '</table>';

                 $("#tbldataview").html(str);
                });
        }


        this.attributtesSection = function(section_id){

            var data = $.param({
                division_id : $scope.railway_division,
                section_id :    section_id
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/rdivisionDetail.php?secid='+section_id+'&levelid=section', data, config)
                .success(function(data){
                    list.secAttributes = data;
                    globalData=data;
                    $('#infoheading').html("Section Info");
                    $("#tbldataview").html();
                    var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                        '<tbody>';
                    for(var i=0;i<data.length;i++) {
                        str = str + '<tr>' +
                        '<th>Section</th>' +
                        '<td>'+data[i].sec_code+'</td>'+
                        '</tr>'+
                        '<tr>' +
                        '<th>Total Moza</th>'+
                        '<td>'+data[i].moza_count+'</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<th>Total Landplan</th>'+
                        '<td>'+data[i].lp_count+'</td>' +
                        '</tr>' +
                        '<tr>' +
                        '<th>Total Parcel</th>'+
                        '<td>'+data[i].parcel_count+'</td>' +
                        '</tr>'+
                        '<tr>' +
                        '<th>Total Yardplan</th>'+
                        '<td>'+data[i].yardplan+'</td>' +
                        '</tr>'+
                        //'<tr>' +
                        //'<th>Total Siding</th>'+
                        //'<td>'+data[i].siding+'</td>' +
                        //'</tr>'+
                        '<tr class="danger">' +
                        '<th colspan="2" style="text-align: center"> Area Info <select onchange="unitConversion(value)"><option>Sq Yrd</option><option>Sq Feet</option><option>Acres</option><option>Kanals</option><option>Marlas</option></select> </th>'+
                        '</tr>'+
                        '<tr class="danger">' +
                        '<th>Landplan</th>'+
                        '<td id="lp">'+commaSeparateNumber(data[i].lp_area)+'</td>' +
                        '</tr>'+
                        '<tr class="danger">' +
                        '<th>ROR Permanent</th>'+
                        '<td id="mh">'+commaSeparateNumber(data[i].mh)+'</td>' +
                        '</tr>'+
                        '<tr class="danger">' +
                        '<th>ROR Periodic</th>'+
                        '<td id="rh">'+commaSeparateNumber(data[i].rh)+'</td>' +
                        '</tr>'+
                        '<tr>'+
                        '<th>Area Comparison Chart</th>'+
                        '<td>  <a href=javascript:areaComparisionChart('+data[i].lp_area+','+data[i].mh+','+data[i].rh+')>View Chart</a></td>'+
                        '</tr>';
                    }
                  //  if(data[0].landplan>0) {
                        $scope.landplanBox = true
                  //  }
                    if(data[0].yardplan >0){
                        $scope.yardplanBox = true
                        $http.post('services/GetComboesData.php?ADMIN_LEVEL=yardplan_lp&LEVEL_ID='+section_id)
                            .success(function(data){
                                list.yardplanlp = data;
                            });
                    }
                    if(data[0].siding>0){
                        $scope.sidingBox = true
                        $scope.sidingparcelBox = true
                        $http.post('services/GetComboesData.php?ADMIN_LEVEL=siding&LEVEL_ID='+section_id)
                            .success(function(data){
                                list.sidings = data;
                            });
                    }
                    if(data[0].parcel_count>0){
                        $scope.parcelBox = true
                    }
                    str=str+'</tbody>'+
                    '</table>';

                    $("#tbldataview").html(str);
                });
        }


        this.sectionSelect = function(section_id,extent,name){
            if(extent!='') {
                list.attributtesSection(section_id);
                zoomToExtent(extent);
                selectFeature("Railway Vector Data", 18, "sec_code", section_id);
                rwySec = name;
            }
            var data = $.param({
                division_id : $scope.railway_division,
                section_id :    section_id
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=landplan&LEVEL_ID='+section_id, data, config)
                .success(function(data){
                    list.landplans = data;
                });

            //  if(list.secAttributes[0].yardplan >0){
            //      $http.post('services/GetComboesData.php?ADMIN_LEVEL=yardplan_lp&LEVEL_ID='+section_id, data, config)
            //          .success(function(data){
            //              list.yardplanlp = data;
            //          });
            //  }
            //
            //if(list.secAttributes[0].siding >0){
            //    $http.post('services/GetComboesData.php?ADMIN_LEVEL=siding&LEVEL_ID='+section_id, data, config)
            //        .success(function(data){
            //            list.sidings = data;
            //        });
            //}
            //$http.post('services/GetComboesData.php?ADMIN_LEVEL=yardplan&LEVEL_ID='+section_id, data, config)
            //    .success(function(data){
            //        list.yardplans = data;
            //    });
            //
            //
            //$http.post('services/GetComboesData.php?ADMIN_LEVEL=siding&LEVEL_ID='+section_id, data, config)
            //    .success(function(data){
            //        list.sidings = data;
            //    });

        }

        this.yardplanSelect=function(yp_sheet,moza){

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=yardplan&LEVEL_ID='+yp_sheet)
                .success(function(data){

                    zoomToExtent(data[0].extent);
                    selectFeature("Railway Vector Data", 10, "yp_sheet", yp_sheet);
                    $('#infoheading').html("YardPlan Info");
                    $("#tbldataview").html();
                    var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                        '<tbody>';
                    for(var i=0;i<data.length;i++) {
                        str = str + '<tr>' +
                        '<th>Station</th>' +
                        '<td>'+data[i].station+'</td>'+
                        '</tr>'+
                        '<tr>' +
                        '<th>Mileage</th>'+
                        '<td>'+data[i].mileage+'</td>' +
                        '</tr>'+
                        '<th>Moza</th>'+
                        '<td>'+moza+'</td>' +
                        '</tr>'


                    }
                    str=str+'</tbody>'+
                    '</table>';

                    $("#tbldataview").html(str);

                });


        }


        this.sidingSelect=function(sheet,moza,station,extent){

                    zoomToExtent(extent);
                    selectFeature("Railway Vector Data", 23, "lp_sheet", sheet);
                    $('#infoheading').html("Siding Info");
                    $("#tbldataview").html();
            var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                        '<tbody>';
                  //  for(var i=0;i<data.length;i++) {
                        str = str + '<tr>' +
                        '<th>Station</th>' +
                        '<td>'+station+'</td>'+
                        '</tr>'+
                        '<tr>' +
                        '<th>Sheet</th>'+
                        '<td>'+sheet+'</td>' +
                        '</tr>'+
                        '<th>Moza</th>'+
                        '<td>'+moza+'</td>' +
                        '</tr>'

                 //   }
                    str=str+'</tbody>'+
                    '</table>';

                    $("#tbldataview").html(str);

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=siding_parcel&LEVEL_ID='+sheet)
                .success(function(data){
                    list.sidingparcels = data;
                });

        }



        this.sidingparcelSelect=function(pin,landuse,moza,station,extent){

            zoomToExtent(extent);
            selectFeature("Railway Vector Data", 22, "pin", pin);
            $('#infoheading').html("Siding Parcel Info");
            $("#tbldataview").html();
            var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                '<tbody>';
            //  for(var i=0;i<data.length;i++) {
            str = str + '<tr>' +
            '<th>Station</th>' +
            '<td>'+station+'</td>'+
            '</tr>'+
            '<tr>' +
            '<th>Landuse</th>'+
            '<td>'+landuse+'</td>' +
            '</tr>'+
            '<th>Moza</th>'+
            '<td>'+moza+'</td>' +
            '</tr>'

            //   }
            str=str+'</tbody>'+
            '</table>';

            $("#tbldataview").html(str);

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=siding_parcel&LEVEL_ID='+sheet)
                .success(function(data){
                    list.sidingparcels = data;
                });

        }



        $scope.landplanSelect1 = function(landplan_id,extent){
            zoomToExtent(extent);
            selectFeature("Railway Vector Data", 11, "lp_sheet", landplan_id);
            // list.attributtesSection(section_id);

            var data = $.param({
                division_id : $scope.railway_division,
                landplan_id :    landplan_id
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=parcel&LEVEL_ID='+landplan_id, data, config)
                .success(function(data){
                    list.parcels = data;
                });

            $scope.lpchainageBox=true;

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=chainage&LEVEL_ID='+landplan_id, data, config)
                .success(function(data){
                    list.chainage = data;
                });


            $http.post('services/rdivisionDetail.php?lp='+landplan_id+'&levelid=landplan', data, config)
                .success(function(data) {
                    list.landplans1 = data;
                    globalData = data;
                    //    alert($scope.railway_division);
                    //   alert('http://202.166.167.126/Revenue Record/'+data[0].moza+'/ROR Permanent/');
                    $http.post('services/getImages.php?FOLDER=../../Revenue Record/' + data[0].moza + '/ROR Permanent', data, config)
                        .success(function (dataP) {
                            list.pictures = dataP;

                            $http.post('services/getImages.php?FOLDER=../../Revenue Record/' + data[0].moza + '/ROR Periodic', data, config)
                                .success(function (dataP1) {
                                    list.pictures1 = dataP1;

                                    $http.post('services/GetComboesData.php?ADMIN_LEVEL=khasra&moza=' +encodeURIComponent(data[0].moza) + '&LEVEL_ID=0', data, config)
                                        .success(function (khasra) {
                                            //alert(khasra)

                                            $('#infoheading').html("Landplan Info");
                                            $("#tbldataview").html();
                                            var strK="";
                                            var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                                                '<tbody>';
                                            for (var i = 0; i < data.length; i++) {
                                                var mozaName = data[i].moza.trim();
                                                //    alert(mozaName);
                                                str = str + '<tr>' +
                                                '<th>Landplan</th>' +
                                                '<td>' + data[i].lp_sheet + '</td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                '<th>Total Chainage</th>' +
                                                '<td>' + data[i].ch_count + '</td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                '<th>Total Parcel</th>' +
                                                '<td>' + data[i].parcel_count + '</td>' +
                                                '</tr>' +
                                                "<tr class='danger'>" +
                                                "<th colspan='2' style='text-align: center'> Area Info <select onchange='unitConversion(value)'><option>Sq Yrd</option><option>Sq Feet</option><option>Acres</option><option>Kanals</option><option>Marlas</option></select>   </th>" +
                                                "</tr>" +
                                                "<tr class='danger'>" +
                                                "<th>Landplan Area</th>" +
                                                "<td id='lp'>" + commaSeparateNumber(data[i].lp_area) + "</td>" +
                                                "</tr>" +
                                                "<tr class='danger'>" +
                                                "<th>ROR Permanent</th>" +
                                                "<td id='mh'>" + commaSeparateNumber(data[i].mh) + "</td>" +
                                                "</tr>" +
                                                "<tr class='danger'>" +
                                                '<th>ROR Periodic</th>' +
                                                '<td id="rh">' + commaSeparateNumber(data[i].rh) + '</td>' +
                                                '</tr>' +

                                                "<tr class='success'>" +
                                                "<th colspan='2' style='text-align: center'> Mileage: </th>" +
                                                "</tr>" +
                                                "<tr class='success'>" +
                                                "<th>Mileage From</th>" +
                                                "<td>" + data[i].f + "</td>" +
                                                "</tr>" +
                                                "<tr class='success'>" +
                                                "<th>Mileage To</th>" +
                                                "<td>" + data[i].t + "</td>" +
                                                "</tr>"+

                                                '<tr>' +
                                                '<th> Download Shapefile</th>' +
                                                '<td><a href=javascript:angular.element($("#infodivs")).scope().downloadShapefile("' + landplan_id + '")>Download</td>' +
                                                '</tr>' +

                                                '<tr>' +
                                                '<th> Download PDF</th>' +
                                                '<td><a href=javascript:angular.element($("#infodivs")).scope().downloadPDF(' + '"' + dataP[0] + '"' + ',' + '"' + dataP1[0] + '"' + ',' + '"' + landplan_id + '"' + ',' + '"' + rwyDiv + '"' + ',' + '"' + encodeURIComponent(rwySec) + '"' + ')>Download PDF</td>' +
                                                    //"<td><a href=javascript:angular.element($('#infodivs')).scope().downloadPDF("+"'"+data[i].lp_sheet+"'"+","+"'"+data[i].ch_count+"'"+","+"'"+data[i].parcel_count+"'"+",'"+mozaName+"')>Download PDF</td>"+
                                                    //"<td><a href=javascript:angular.element($('#infodivs')).scope().downloadPDF("+"'"+data[i].lp_sheet+"'"+","+"'"+data[i].ch_count+"'"+","+"'"+data[i].parcel_count+"'"+","+data[i].moza+")>Download PDF</td>"+
                                                '</tr>' +


                                                "<tr>" +
                                                '<th>ROR Permanent Image</th>' +
                                                    //  '<td>'+"<td><a href='http://202.166.167.126/Revenue Record/"+data[0].moza+"/ROR Permanent/"+dataP[1]+"' target='_blank'> Click to View Record</a>"+'</td>' +
                                                "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/Revenue Record/" + data[0].moza + "/ROR Permanent/" + dataP[0] + "'  target='_blank'>";
                                                if (dataP == "" || dataP == 'undefined') {
                                                    str = str + "No Image found";
                                                } else {
                                                    str = str + "View Permanent Record";

                                                }

                                                str = str + "</a>" + "</td>" +
                                                "<tr>" +
                                                '<th>ROR Preodic Image</th>' +
                                                "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/Revenue Record/" + data[0].moza + "/ROR Periodic/" + dataP1[0] + "'  target='_blank'>";
                                                if (dataP1 == "" || dataP1 == 'undefined') {
                                                    str = str + "No Image found";
                                                } else {
                                                    str = str + "View Periodic Record";

                                                }
                                                str = str + "</a>" + "</td>" +
                                                '</tr>' +
                                                '<tr>' +
                                                '<th>Area Comparison Chart</th>' +
                                                '<td>  <a href=javascript:areaComparisionChart(' + data[i].lp_area + ',' + data[i].mh + ',' + data[i].rh + ')>View Chart</a></td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                '<th> Mauza</th>' +
                                                '<td>' + data[i].moza + '</td>' +
                                                '</tr>'+
                                                "<tr class='info'>" +
                                                "<th colspan='2' style='text-align: center'> Khasra Info: </th>" +
                                                "</tr>" +
                                                "<tr class='info'>" +
                                                "<th>Khasra</th>" +
                                                "<th>Sub Khasra</th>" +
                                                "</tr>";
                                                //'<tr>'+
                                                //'<th>Khasra</th>'+'<td>';
                                                for(var k=0;k<khasra.length;k++) {
                                                    str=str+"<tr class='info'>" +
                                                    "<td>"+'<a href=javascript:zoomToKhasra('+"'"+khasra[k].extent+"'"+','+khasra[k].khasra_no+')>'+khasra[k].khasra_no+'</a>'+"</td>" +
                                                    "<td>" +khasra[k].sub_khasra+ "</td>" +
                                                    "</tr>";
                                                    // '<a href=javascript:zoomToKhasra('+"'"+khasra[k].extent+"'"+','+khasra[k].khasra_no+')>'+khasra[k].khasra_no+'</a>';
                                                }
                                                //  strK=strK.substring(0, strK.length-1);
                                                //  str = str+strK +'</tr>';
                                            }
                                            str = str +'</td>'+'</tbody>' +
                                            '</table>';

                                            $("#tbldataview").html(str);

                                        });

                                });

                        });
                });
        }


        this.landplanSelect = function(landplan_id,extent){
            zoomToExtent(extent);
            selectFeature("Railway Vector Data", 11, "lp_sheet", landplan_id);
           // list.attributtesSection(section_id);

            var data = $.param({
                division_id : $scope.railway_division,
                landplan_id :    landplan_id
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=parcel&LEVEL_ID='+landplan_id, data, config)
                .success(function(data){
                    list.parcels = data;
					var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                        '<tbody>'+
                         '<th>Id</th>'+
                          '<th>Survey Type</th>'+
                           '<th>Pin</th>'
                    for(var i=0;i<data.length;i++) {
                        str = str +'<tr ng-click="filterCtrlparcelSelect('+"'"+data[i].old_pin+"'"+','+"'"+data[i].xmin+"'"+','+"'"+data[i].ymin+"'"+','+"'"+data[i].xmax+"'"+','+"'"+data[i].ymax+"'"+','+"'"+data[i].survey_typ+"'"+','+"'"+data[i].sub_parcel+"'"+','+"'"+data[i].unique_id+"'"+','+"'"+data[i].gid+"'"+')">' +
                        '<td >'+data[i].gid+'</td>'+
                        '<td >'+data[i].survey_typ+'</td>' +
                        '<td>'+data[i].old_pin+'</td>' +
                        '</tr>'

                    }
                    str=str+'</tbody>'+
                    '</table>';
                     var strCompile=$compile(str)($scope);
                    $("#parcelInfoTabs").html(strCompile);
                });

                });

            $scope.lpchainageBox=true;

            $http.post('services/GetComboesData.php?ADMIN_LEVEL=chainage&LEVEL_ID='+landplan_id, data, config)
                .success(function(data){
                    list.chainage = data;
                });


            $http.post('services/rdivisionDetail.php?lp='+landplan_id+'&levelid=landplan', data, config)
                .success(function(data) {
                    list.landplans1 = data;
                    globalData = data;
                    //    alert($scope.railway_division);
                    //   alert('http://202.166.167.126/Revenue Record/'+data[0].moza+'/ROR Permanent/');
                    $http.post('services/getImages.php?FOLDER=../../Revenue Record/' + data[0].moza + '/ROR Permanent', data, config)
                        .success(function (dataP) {
                            list.pictures = dataP;

                            $http.post('services/getImages.php?FOLDER=../../Revenue Record/' + data[0].moza + '/ROR Periodic', data, config)
                                .success(function (dataP1) {
                                    list.pictures1 = dataP1;

                                    $http.post('services/GetComboesData.php?ADMIN_LEVEL=khasra&moza=' +encodeURIComponent(data[0].moza) + '&LEVEL_ID=0', data, config)
                                        .success(function (khasra) {
                                            //alert(khasra)

                                            $('#infoheading').html("Landplan Info");
                                            $("#tbldataview").html();
                                            var strK="";
                                            var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                                                '<tbody>';
                                            for (var i = 0; i < data.length; i++) {
                                                var mozaName = data[i].moza.trim();
                                                //    alert(mozaName);
                                                str = str + '<tr>' +
                                                '<th>Landplan</th>' +
                                                '<td>' + data[i].lp_sheet + '</td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                '<th>Total Chainage</th>' +
                                                '<td>' + data[i].ch_count + '</td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                '<th>Total Parcel</th>' +
                                                '<td>' + data[i].parcel_count + '</td>' +
                                                '</tr>' +
                                                "<tr class='danger'>" +
                                                "<th colspan='2' style='text-align: center'> Area Info <select onchange='unitConversion(value)'><option>Sq Yrd</option><option>Sq Feet</option><option>Acres</option><option>Kanals</option><option>Marlas</option></select>   </th>" +
                                                "</tr>" +
                                                "<tr class='danger'>" +
                                                "<th>Landplan Area</th>" +
                                                "<td id='lp'>" + commaSeparateNumber(data[i].lp_area) + "</td>" +
                                                "</tr>" +
                                                "<tr class='danger'>" +
                                                "<th>ROR Permanent</th>" +
                                                "<td id='mh'>" + commaSeparateNumber(data[i].mh) + "</td>" +
                                                "</tr>" +
                                                "<tr class='danger'>" +
                                                '<th>ROR Periodic</th>' +
                                                '<td id="rh">' + commaSeparateNumber(data[i].rh) + '</td>' +
                                                '</tr>' +

                                                "<tr class='success'>" +
                                                "<th colspan='2' style='text-align: center'> Mileage: </th>" +
                                                "</tr>" +
                                                "<tr class='success'>" +
                                                "<th>Mileage From</th>" +
                                                "<td>" + data[i].f + "</td>" +
                                                "</tr>" +
                                                "<tr class='success'>" +
                                                "<th>Mileage To</th>" +
                                                "<td>" + data[i].t + "</td>" +
                                                "</tr>"+

                                                '<tr>' +
                                                '<th> Download Shapefile</th>' +
                                                '<td><a href=javascript:angular.element($("#infodivs")).scope().downloadShapefile("' + landplan_id + '")>Download</td>' +
                                                '</tr>' +

                                                '<tr>' +
                                                '<th> Download PDF</th>' +
                                                '<td><a href=javascript:angular.element($("#infodivs")).scope().downloadPDF(' + '"' + dataP[0] + '"' + ',' + '"' + dataP1[0] + '"' + ',' + '"' + landplan_id + '"' + ',' + '"' + rwyDiv + '"' + ',' + '"' + encodeURIComponent(rwySec) + '"' + ')>Download PDF</td>' +
                                                    //"<td><a href=javascript:angular.element($('#infodivs')).scope().downloadPDF("+"'"+data[i].lp_sheet+"'"+","+"'"+data[i].ch_count+"'"+","+"'"+data[i].parcel_count+"'"+",'"+mozaName+"')>Download PDF</td>"+
                                                    //"<td><a href=javascript:angular.element($('#infodivs')).scope().downloadPDF("+"'"+data[i].lp_sheet+"'"+","+"'"+data[i].ch_count+"'"+","+"'"+data[i].parcel_count+"'"+","+data[i].moza+")>Download PDF</td>"+
                                                '</tr>' +


                                                "<tr>" +
                                                '<th>ROR Permanent Image</th>' +
                                                    //  '<td>'+"<td><a href='http://202.166.167.126/Revenue Record/"+data[0].moza+"/ROR Permanent/"+dataP[1]+"' target='_blank'> Click to View Record</a>"+'</td>' +
                                                "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/Revenue Record/" + data[0].moza + "/ROR Permanent/" + dataP[0] + "'  target='_blank'>";
                                                if (dataP == "" || dataP == 'undefined') {
                                                    str = str + "No Image found";
                                                } else {
                                                    str = str + "View Permanent Record";

                                                }

                                                str = str + "</a>" + "</td>" +
                                                "<tr>" +
                                                '<th>ROR Preodic Image</th>' +
                                                "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/Revenue Record/" + data[0].moza + "/ROR Periodic/" + dataP1[0] + "'  target='_blank'>";
                                                if (dataP1 == "" || dataP1 == 'undefined') {
                                                    str = str + "No Image found";
                                                } else {
                                                    str = str + "View Periodic Record";

                                                }
                                                str = str + "</a>" + "</td>" +
                                                '</tr>' +
                                                '<tr>' +
                                                '<th>Area Comparison Chart</th>' +
                                                '<td>  <a href=javascript:areaComparisionChart(' + data[i].lp_area + ',' + data[i].mh + ',' + data[i].rh + ')>View Chart</a></td>' +
                                                '</tr>' +
                                                '<tr>' +
                                                '<th> Mauza</th>' +
                                                '<td>' + data[i].moza + '</td>' +
                                                '</tr>'+
                                                "<tr class='info'>" +
                                                "<th colspan='2' style='text-align: center'> Khasra Info: </th>" +
                                                "</tr>" +
                                                "<tr class='info'>" +
                                                "<th>Khasra</th>" +
                                                "<th>Sub Khasra</th>" +
                                                "</tr>";
                                                //'<tr>'+
                                                //'<th>Khasra</th>'+'<td>';
                                                for(var k=0;k<khasra.length;k++) {
                                                    str=str+"<tr class='info'>" +
                                                    "<td>"+'<a href=javascript:zoomToKhasra('+"'"+khasra[k].extent+"'"+','+khasra[k].khasra_no+')>'+khasra[k].khasra_no+'</a>'+"</td>" +
                                                    "<td>" +khasra[k].sub_khasra+ "</td>" +
                                                    "</tr>";
                                                       // '<a href=javascript:zoomToKhasra('+"'"+khasra[k].extent+"'"+','+khasra[k].khasra_no+')>'+khasra[k].khasra_no+'</a>';
                                                }
                                              //  strK=strK.substring(0, strK.length-1);
                                              //  str = str+strK +'</tr>';
                                            }
                                            str = str +'</td>'+'</tbody>' +
                                            '</table>';

                                            $("#tbldataview").html(str);

                                        });

                                });

                        });
                });
        }







        this.chainageSelect=function(ch_val,symbology,xmin,ymin,xmax,ymax){
            extent=xmin+","+ymin+","+xmax+","+ymax;
          //  alert(symbology);
            zoomToExtent(extent);
            selectFeature("Railway Vector Data",1, "ch_val", ch_val,symbology);
            //$('#infoheading').html("Chainage Info");
            //var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
            //    '<tbody>';
            //str = str + '<tr>' +
            //'<th>Chanage</th>' +
            //'<td>'+ch_val+'</td>'+
            //'</tr>'+
            //'<tr>' +
            //'<th>Symbology</th>'+
            //'<td>'+symbology+'</td>' +
            //'</tr>'
            //
            ////   }
            //str=str+'</tbody>'+
            //'</table>';
            //
            //$("#tbldataview").html(str);
        }

        this.parcelSelect = function(parcel_pin,xmin,ymin,xmax,ymax,survey_typ,sub_id,unique_id,gid){
            if(xmin!=0&&ymin!=0&&xmax!=0&&ymax!=0) {
                extent = xmin + "," + ymin + "," + xmax + "," + ymax;
                zoomToExtent(extent);
                selectFeature("Railway Vector Data", 7, "unique_id", unique_id);
            }
            $scope.setDropdownVal={
                pin:gid
            };
           // alert($scope.setDropdownVal.pin);
            // list.attributtesSection(section_id);

            var data = $.param({
                division_id : $scope.railway_division,
                landplan_id :    parcel_pin
            });

            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            }

            $http.post('services/rdivisionDetail.php?pin='+parcel_pin+'&levelid=parcel&survey_typ='+survey_typ+"&sub_parcel="+sub_id, data, config)
                .success(function(data){
                //    list.parcelInfo = data;

                    if(survey_typ=='Service Building') {

                        $http.post('services/getImages.php?FOLDER=../../pr_stat/'+ data[0].image_directory ,data,config)
                            .success(function(imgdata) {

                        $('#infoheading').html(survey_typ);
                                $("#tbldataview").html();
                                var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                            '<tbody>';
                        for (var i = 0; i < data.length; i++) {
                            str = str + '<tr>' +
                            '<th>Covered Area(sqft) </th>' +
                            '<td>' + data[i].covered_area_sq_ft + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Description Of Building</th>' +
                            '<td>' + data[i].description_of_building + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>District</th>' +
                            '<td>' + data[i].district_name + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Division</th>' +
                            '<td>' + data[i].division+ '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>From Km</th>' +
                            '<td>' + data[i].from_km + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>To Km</th>' +
                            '<td>' + data[i].to_km + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Moza</th>' +
                            '<td>' + data[i].mauza_name + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<tr>' +
                            '<th>Khasra No</th>' +
                            '<td>' + data[i].khasra_no + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Parcel Sub Division </th>' +
                            '<td>' + data[i].parcel_sub_division + '</td>' +
                            '</tr>' +
                            '<th>Station Name</th>' +
                            '<td>' + data[i].station_name + '</td>' +
                            '</tr>';
                                    for(var j=0;j<imgdata.length;j++) {
                                        if(j==0) {
                                            str = str + "<tr>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[j] + "'  target='_blank'>click here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }else{
                                            str = str + "<tr style='visibility: hidden'>" +
                                            '<th>ROR Permanent Image</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[j] + "'  target='_blank'>click here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }
                                    }

                        }
                        str = str + '</tbody>' +
                        '</table>';

                        $("#tbldataview").html(str);
                            });
                    }else  if(survey_typ=='Government Department') {

                        $http.post('services/getImages.php?FOLDER=../../pr_stat/'+ data[0].image_directory ,data,config)
                            .success(function(imgdata) {

                                $('#infoheading').html(survey_typ);
                                $("#tbldataview").html();
                                var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                                    '<tbody>';
                                for (var i = 0; i < data.length; i++) {
                                    str = str + '<tr>' +
                                    '<th>Covered Area(sqft) </th>' +
                                    '<td>' + data[i].covered_area_sq_ft + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Department Name</th>' +
                                    '<td>' + data[i].department_name + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Parcel Sub Division</th>' +
                                    '<td>' + data[i].parcel_sub_division + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Description of Land</th>' +
                                    '<td>' + data[i].description_of_land + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Area Under Utilization(sqft)</th>' +
                                    '<td>' + data[i].area_under_utilization_sq_ft + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Agreement Date</th>' +
                                    '<td>' + data[i].agreement_date + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Muza Name</th>' +
                                    '<td>' + data[i].mauza_name + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Khasra No</th>' +
                                    '<td>' + data[i].khasra_no + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>District</th>' +
                                    '<td>' + data[i].district_name + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Division</th>' +
                                    '<td>' + data[i].division + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>From Km</th>' +
                                    '<td>' + data[i].from_km + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>To Km</th>' +
                                    '<td>' + data[i].to_km + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Station Name</th>' +
                                    '<td>' + data[i].station_name + '</td>' +
                                    '</tr>';
                                    for (var j = 0; j < imgdata.length; j++) {
                                        if(j==0) {
                                            str = str + "<tr>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[j] + "'  target='_blank'>Click Here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }else{
                                            str = str + "<tr style='display: none'>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[j] + "'  target='_blank'>Click Here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }
                                    }
                                }
                                str = str + '</tbody>' +
                                '</table>';

                                $("#tbldataview").html(str);
                            });
                    }
                    else  if(survey_typ=='Encroachment') {

                        $http.post('services/getImages.php?FOLDER=../../pr_stat/'+ data[0].image_directory ,data,config)
                            .success(function(imgdata) {

                                $('#infoheading').html(survey_typ);
                                $("#tbldataview").html();
                                var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                                    '<tbody>';
                                for (var i = 0; i < data.length; i++) {
                                    str = str + '<tr>' +
                                    '<th>Encroached Area(sqft) </th>' +
                                    '<td>' + data[i].encroached_area_sq_ft + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Date Of Encroachment</th>' +
                                    '<td>' + data[i].date_of_encroachment + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<tr>' +
                                    '<th>Description Of Encroachment</th>' +
                                    '<td>' + data[i].description_of_encroachment + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Encroacher Name</th>' +
                                    '<td>' + data[i].encroacher_name + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<tr>' +
                                    '<th>Muza Name</th>' +
                                    '<td>' + data[i].mauza_name + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<tr>' +
                                    '<th>Khasra No</th>' +
                                    '<td>' + data[i].khasra_no + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>District</th>' +
                                    '<td>' + data[i].district_name + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Division</th>' +
                                    '<td>' + data[i].division + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>From km</th>' +
                                    '<td>' + data[i].from_km + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>To km</th>' +
                                    '<td>' + data[i].to_km + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Station Name</th>' +
                                    '<td>' + data[i].station_name + '</td>' +
                                    '</tr>';
                                    for (var i = 0; i < imgdata.length; i++) {
                                        if(i==0) {
                                            str = str + "<tr>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[i] + "'  target='_blank'>Click Here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }else{
                                            str = str + "<tr style='display: none'>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[i] + "'  target='_blank'>Click Here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }
                                    }
                                }
                                str = str + '</tbody>' +
                                '</table>';

                                $("#tbldataview").html(str);
                            })
                    }
                    else  if(survey_typ=='Katchi Abadi') {
                        $http.post('services/getImages.php?FOLDER=../../pr_stat/'+ data[0].image_directory ,data,config)
                            .success(function(imgdata) {
                        $('#infoheading').html(survey_typ);
                                $("#tbldataview").html();
                                var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                            '<tbody>';
                        for (var i = 0; i < data.length; i++) {
                            str = str + '<tr>' +
                            '<th>Covered Area(sqft) </th>' +
                            '<td>' + data[i].covered_area_sq_ft + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Area Under Occupation</th>' +
                            '<td>' + data[i].area_under_occupation_sq_ft + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Occupant Name</th>' +
                            '<td>' + data[i].occupant_name+ '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Road Name</th>' +
                            '<td>' + data[i].road_name+ '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Station Name</th>' +
                            '<td>' + data[i].station_name+ '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<tr>' +
                            '<th>Muza Name</th>' +
                            '<td>' + data[i].mauza_name+ '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Khasra No</th>' +
                            '<td>' + data[i].khasra_no+ '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Status</th>' +
                            '<td>' + data[i].status+ '</td>' +
                            '</tr>' +
                            '<th>District</th>' +
                            '<td>' + data[i].district_name + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Division</th>' +
                            '<td>' + data[i].division+ '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>From km</th>' +
                            '<td>' + data[i].from_km + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>To km</th>' +
                            '<td>' + data[i].to_km + '</td>' +
                            '</tr>' +
                            '<tr>' +
                            '<th>Station Name</th>' +
                            '<td>' + data[i].station_name + '</td>' +
                            '</tr>';
                            for (var i = 0; i < imgdata.length; i++) {
                                if(i==0) {
                                    str = str + "<tr>" +
                                    '<th>Images</th>' +
                                    "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[i] + "'  target='_blank'>Click Here" +
                                    "</a>" + "</td>" +
                                    "</tr>"
                                }else{
                                    str = str + "<tr style='display: none'>" +
                                    '<th>Images</th>' +
                                    "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[i] + "'  target='_blank'>Click Here" +
                                    "</a>" + "</td>" +
                                    "</tr>"
                                }
                            }
                        }
                        str = str + '</tbody>' +
                        '</table>';

                        $("#tbldataview").html(str);
                            })
                    }else  if(survey_typ=='Leased Area For Shops') {
                        $http.post('services/getImages.php?FOLDER=../../pr_stat/'+ data[0].image_directory ,data,config)
                            .success(function(imgdata) {
                                $('#infoheading').html(survey_typ);
                                $("#tbldataview").html();
                                var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                                    '<tbody>';
                                for (var i = 0; i < data.length; i++) {
                                    str = str + '<tr>' +
                                    '<th>Area(sqft) </th>' +
                                    '<td>' + data[i].area_sq_ft + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Type Of Lease</th>' +
                                    '<td>' + data[i].type_of_lease + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Name Of Lessee</th>' +
                                    '<td>' + data[i].name_of_lessee+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Cnic No</th>' +
                                    '<td>' + data[i].cnic_no+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Non Refundable Fee</th>' +
                                    '<td>' + data[i].non_refundable_fee+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Annual Rent</th>' +
                                    '<td>' + data[i].annual_rent+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<tr>' +
                                    '<th>Date of Execution</th>' +
                                    '<td>' + data[i].date_of_execution+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Date Of Expiry</th>' +
                                    '<td>' + data[i].date_of_expiry+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Total Lease Period</th>' +
                                    '<td>' + data[i].total_lease_period+ '</td>' +
                                    '</tr>' +
                                    '<th>Approved Plan No</th>' +
                                    '<td>' + data[i].approved_plan_no + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Station Name</th>' +
                                    '<td>' + data[i].station_name+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>From Km</th>' +
                                    '<td>' + data[i].from_km + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>To Km</th>' +
                                    '<td>' + data[i].to_km + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Section Name</th>' +
                                    '<td>' + data[i].section_name + '</td>' +
                                    '</tr>';
                                    for (var i = 0; i < imgdata.length; i++) {
                                        if(i==0) {
                                            str = str + "<tr>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[i] + "'  target='_blank'>Click Here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }else{
                                            str = str + "<tr style='display: none'>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[i] + "'  target='_blank'>Click Here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }
                                    }
                                }
                                str = str + '</tbody>' +
                                '</table>';

                                $("#tbldataview").html(str);
                            })
                    }else  if(survey_typ=='Leased Area (for other than shops)') {
                        $http.post('services/getImages.php?FOLDER=../../pr_stat/'+ data[0].image_directory ,data,config)
                            .success(function(imgdata) {
                                $('#infoheading').html(survey_typ);
                                $("#tbldataview").html();
                                var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                                    '<tbody>';
                                for (var i = 0; i < data.length; i++) {
                                    str = str + '<tr>' +
                                    '<th>Area(sqft) </th>' +
                                    '<td>' + data[i].area_sq_ft + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Type Of Lease</th>' +
                                    '<td>' + data[i].type_of_lease + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Name Of Lessee</th>' +
                                    '<td>' + data[i].name_of_lessee+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Cnic No</th>' +
                                    '<td>' + data[i].cnic_no+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Dc Price Area</th>' +
                                    '<td>' + data[i].dc_price_area+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Lease Rate</th>' +
                                    '<td>' + data[i].lease_rate+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Date Of Execution</th>' +
                                    '<td>' + data[i].date_of_execution+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Date Of Expiry</th>' +
                                    '<td>' + data[i].date_of_expiry+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Station Name</th>' +
                                    '<td>' + data[i].station_name+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>From Km</th>' +
                                    '<td>' + data[i].from_km + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>To Km</th>' +
                                    '<td>' + data[i].to_km + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Section Name</th>' +
                                    '<td>' + data[i].section_name + '</td>' +
                                    '</tr>';
                                    for (var i = 0; i < imgdata.length; i++) {
                                        if(i==0) {
                                            str = str + "<tr>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[i] + "'  target='_blank'>Click Here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }else{
                                            str = str + "<tr style='display: none'>" +
                                            '<th>Images</th>' +
                                            "<td>" + "<a class='fancybox-button' rel='fancybox-button' href='http://202.166.167.126/pr_stat/" + data[0].image_directory + "/" + imgdata[i] + "'  target='_blank'>Click Here" +
                                            "</a>" + "</td>" +
                                            "</tr>"
                                        }
                                    }
                                }
                                str = str + '</tbody>' +
                                '</table>';

                                $("#tbldataview").html(str);
                            })
                    }else  if(survey_typ=='Open Land') {
                        $http.post('services/getImages.php?FOLDER=../../pr_stat/'+ data[0].image_directory ,data,config)
                            .success(function(imgdata) {
                                $('#infoheading').html(survey_typ);
                                $("#tbldataview").html();
                                var str = '<table class="table table-striped table-bordered" style="margin: 0 0 0 0">' +
                                    '<tbody>';
                                for (var i = 0; i < data.length; i++) {
                                    str = str + '<tr>' +
                                    '<th> Section</th>' +
                                    '<td>' + data[i].section_ + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Station</th>' +
                                    '<td>' + data[i].station + '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>District</th>' +
                                    '<td>' + data[i].district+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Tehsil</th>' +
                                    '<td>' + data[i].tehsil+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Moza</th>' +
                                    '<td>' + data[i].moza+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Railway Division</th>' +
                                    '<td>' + data[i].rwy_div+ '</td>' +
                                    '</tr>' +
                                    '<tr>' +
                                    '<th>Landuse</th>' +
                                    '<td>' + data[i].landuse+ '</td>' +
                                    '</tr>'

                                }
                                str = str + '</tbody>' +
                                '</table>';

                                $("#tbldataview").html(str);
                            })
                    }


                })



          //      });
        }

        $scope.downloadShapefile = function(lp_sheet){
            var url = 'http://202.166.167.126:8080/RailwayDownloadServices/shp?lp='+lp_sheet;
            var redirectWindow = window.open(url, '_blank');
            redirectWindow.location;
        }

        $scope.downloadPDF = function(permanent,priodic,lp,div,sec){
            var url = 'http://202.166.167.126/railway_mapviewer/services/CreatePdf.php?lp='+lp+'&permanent='+permanent+'&priodic='+priodic+'&div='+div+'&sec='+sec;
            var redirectWindow = window.open(url);
            redirectWindow.location;
        }

        $scope.autoFillDropDown=function(pin,survey_typ,sub_parcel,gid){
          //  alert(pin+","+survey_typ+","+sub_parcel);
            main.parcelSelect(pin,0,0,0,0,survey_typ,sub_parcel,0,gid)
        }


        $scope.selectMileage=function(div){
        //    alert(div);
            $http.post('services/GetComboesData.php?ADMIN_LEVEL=mileage&LEVEL_ID=0&division='+div)
                .success(function(data){
                    list.mileageFrom = data;
                    list.mileageTo=data;
                });
        }

        $scope.selectLandplanFromMileage=function(){
                var from=$scope.mileage_from;
            $timeout(function() {
            $http.post('services/GetComboesData.php?ADMIN_LEVEL=mileage_landplan&LEVEL_ID=0&from='+from+'&to='+$scope.mileage_to+'&div='+rwyDivId)
                .success(function(data){
                    list.landplans = data;
                });
            }, 3000);
        }


        $scope.revStat=function(){
            $http({
                method: 'GET',
                url: 'services/rdivisionDetail.php?levelid=revStat'
            }).success(function(data) {
                $('#infoheading').html("Revenue Statistics");
                $("#tbldataview").html();
                var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                    '<tbody>';
                for(var i=0;i<data.length;i++) {
                    str = str + '<tr>' +
                    '<th>Total Province</th>' +
                    '<td>'+data[i].pro+'</td>'+
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Division</th>'+
                    '<td>'+data[i].div+'</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<th>Total District</th>'+
                    '<td>'+data[i].distt+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Tehsil</th>'+
                    '<td>'+data[i].teh+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Moza</th>'+
                    '<td>'+data[i].moza+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Railway Division</th>'+
                    '<td>'+data[i].rwy_div+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Railway Section</th>'+
                    '<td>'+data[i].sec+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Landplan</th>'+
                    '<td>'+data[i].landplan+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Yardplan</th>'+
                    '<td>'+data[i].yardplan+'</td>' +
                    '</tr>'+
                    //'<tr>' +
                    //'<th>Total Siiding</th>'+
                    //'<td>'+data[i].siding+'</td>' +
                    //'</tr>'+
                    '<tr>' +
                    '<th>Total Parcel</th>'+
                    '<td>'+data[i].parcel+'</td>' +
                    '</tr>'



                }
                str=str+'</tbody>'+
                '</table>';

                $("#tbldataview").html(str);

            });

        }

        $scope.switchGraphic=function(){
            map.graphics.clear();
           // var location = [73, 31];
          //  map.centerAndZoom(location, 6);
            $scope.railway_section="";
            $scope.railway_division="";
            $scope.railway_landplan="";
            $scope.railway_parcel="";
            $scope.lp_chainage="";
            $scope.mileage_from="";
            $scope.mileage_to="";
            $scope.landplanBox = false;
            $scope.yardplanBox = false;
            $scope.sidingBox = false;
            $scope.sidingparcelBox = false;
            $scope.parcelBox = false;
            $scope.lpchainageBox = false;
            $scope.railway=false;
            $scope.railway=true;
            map.setExtent(initExtent);

            $http({
                method: 'GET',
                url: 'services/rdivisionDetail.php?levelid=totalStats'
            }).success(function(data) {
                $('#infoheading').html("Railway Statistics");
                $("#tbldataview").html();
                var str='<table class="table table-striped table-bordered" style="margin: 0 0 0 0">'+
                    '<tbody>';
                for(var i=0;i<data.length;i++) {
                    str = str + '<tr>' +
                    '<th>Total Division</th>' +
                    '<td>'+data[i].totaldivision+'</td>'+
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Sections</th>'+
                    '<td>'+data[i].totalsection+'</td>' +
                    '</tr>' +
                    '<tr>' +
                    '<th>Total LandPlans</th>'+
                    '<td>'+data[i].totallandplans+'</td>' +
                    '</tr>'+
                    '<tr>' +
                    '<th>Total Yardplans</th>'+
                    '<td>'+data[i].totalyardplans+'</td>' +
                    '</tr>'
                    //'<tr>' +
                    //'<th>Total Sidings</th>'+
                    //'<td>'+data[i].totalsidings+'</td>' +
                    //'</tr>'

                }
                str=str+'</tbody>'+
                '</table>';

                $("#tbldataview").html(str);

            });


        }


        this.districtSelect = function(district){

        }

    }]);

//function commaSeparateNumber(val){
//    while (/(\d+)(\d{3})/.test(val.toString())){
//        val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
//    }
//    return val;
//}

function commaSeparateNumber(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function unitConversion(unit){

    if(unit=="Sq Feet"){
       var lp=globalData[0].lp_area*9;
        var mh=globalData[0].mh*9;
        var rh=globalData[0].rh*9
        $("#lp").html(commaSeparateNumber(lp));
        $("#mh").html(commaSeparateNumber(mh));
        $("#rh").html(commaSeparateNumber(rh));
    }

    if(unit=="Sq Yrd"){
        var lp=globalData[0].lp_area;
        var mh=globalData[0].mh;
        var rh=globalData[0].rh;
        $("#lp").html(commaSeparateNumber(lp));
        $("#mh").html(commaSeparateNumber(mh));
        $("#rh").html(commaSeparateNumber(rh));
    }

    if(unit=="Acres"){
        var lp=globalData[0].lp_area/4840;
        var mh=globalData[0].mh/4840;
        var rh=globalData[0].rh/4840;
        $("#lp").html(commaSeparateNumber(Math.round(lp)));
        $("#mh").html(commaSeparateNumber(Math.round(mh)));
        $("#rh").html(commaSeparateNumber(Math.round(rh)));
    }

    if(unit=="Kanals"){
        var lp=globalData[0].lp_area/605;
        var mh=globalData[0].mh/605;
        var rh=globalData[0].rh/605;
        $("#lp").html(commaSeparateNumber(Math.round(lp)));
        $("#mh").html(commaSeparateNumber(Math.round(mh)));
        $("#rh").html(commaSeparateNumber(Math.round(rh)));
    }

    if(unit=="Marlas"){
        var lp=globalData[0].lp_area/30;
        var mh=globalData[0].mh/30;
        var rh=globalData[0].rh/30;
        $("#lp").html(commaSeparateNumber(Math.round(lp)));
        $("#mh").html(commaSeparateNumber(Math.round(mh)));
        $("#rh").html(commaSeparateNumber(Math.round(rh)));
    }

}

function areaComparisionChart(lparea,parmanent,priodic) {

    // Create the chart
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        credits:false,
        title: {
            text: 'Area Comparison'
        },
//            subtitle: {
//                text: 'Click the columns to view versions. Source: <a href="http://netmarketshare.com">netmarketshare.com</a>.'
//            },
        xAxis: {
            categories:['Landplan Area','ROR Permanent','ROR Periodic'],
            title: {
                text:'Type of Record'
            }
        },
        yAxis: {
            allowDecimals: false,
            min: 0,
            title: {
                text: 'Area in SqYrd'
            }

        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -10,
            y: 100,
            borderWidth: 0
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true
                    //  format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span><br/>'
        },

        series: [{
            name: 'Area',
           // colorByPoint: true,
            data: [{
                name: 'Landplan Area',
                y: lparea
            }, {
                name: 'ROR Parmanent',
                y:parmanent
            }, {
                name: 'ROR Preodic',
                y:priodic
            }]
        }]

    });

    $("#myModal").modal();


}


    function zoomToKhasra(extent,khasra_no){
     //   var ex="'"+extent+"'";
        zoomToExtent(extent);
        selectFeature("Railway Vector Data",9, "khasra_no", khasra_no);
    }







<?php
/**
 * Created by PhpStorm.
 * User: idrees
 * Date: 10/1/2015
 * Time: 3:45 AM
 */
include ('./connection.php');

class GetRevenueArrays extends Connection{

    public $mozaName = null;
    public $tehsilName = null;
    public $districtName = null;
    public $divisionName = null;
    public $provinceName = null;

    public $mozaId = null;
    public $tehsilId = null;
    public  $districtId = null;
    public $divisionId = null;
    public $provinceId = null;

    public $arrRevenueNames = array();
    public $arrMoza = array();
    public $arrTehsil = array();
    public $arrDistrict = array();
    public $arrDivision = array();
    public $arrProvince = array();

    public function GetRevenueArrays(){
        $this->connectionDB();
        $this->getRevenueIds();
    }
    public function  getRevenueIds()
    {
        $mozaName = $_REQUEST['MOZA_ID'];
        $tehsilId = $_REQUEST['TEHSIL_ID'];

        $sql = 'select getreverserevenueids(\''.$mozaName.'\', '.$tehsilId.') as id_list;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $ID_Lists = $row['id_list'];
            $splittedIds = explode(",", $ID_Lists);
            $this->mozaId = $splittedIds[0];
            $this->tehsilId = $splittedIds[1];
            $this->districtId = $splittedIds[2];
            $this->divisionId = $splittedIds[3];
            $this->provinceId = $splittedIds[4];

            $this->mozaName = $splittedIds[5];
            $this->tehsilName = $splittedIds[6];
            $this->districtName = $splittedIds[7];
            $this->divisionName = $splittedIds[8];
            $this->provinceName = $splittedIds[9];
        }
    }

    public  function getRevenueNames()
    {
        $nameActivity = array
        (
            'province' => $this->provinceName,
            'division' => $this->divisionName,
            'district' => $this->districtName,
            'tehsil' => $this->tehsilName,
            'moza' => $this->mozaName
        );
        array_push($this->arrRevenueNames, $nameActivity);
    }

    public function getMozaArray()
    {
        $sql = 'SELECT id, name, tehsil_id, sec_code, extent FROM moza where tehsil_id = '.$this->tehsilId. ' order by name;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $mozaActivity = array
            (
                'id' => $row['id'],
                'name' => $row['name'],
                'tehsil_id' => $row['tehsil_id'],
                'sec_code' => $row['sec_code'],
                'extent' => $row['extent']
            );
            array_push( $this->arrMoza,$mozaActivity);
        }
    }

    public function getTehsilArray()
    {
        $sql = 'SELECT id, name, distt_id, extent FROM tehsil where distt_id = '.$this->districtId. ' order by name;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $tehsilActivity = array
            (
                'id' => $row['id'],
                'name' => $row['name'],
                'distt_id' => $row['distt_id'],
                'extent' => $row['extent']
            );
            array_push( $this->arrTehsil,$tehsilActivity);
        }
    }

    public function getDistrictArray()
    {
        $sql = 'SELECT id, name, div_id, extent FROM district where div_id = '.$this->divisionId. ' order by name;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $districtActivity = array
            (
                'id' => $row['id'],
                'name' => $row['name'],
                'div_id' => $row['div_id'],
                'extent' => $row['extent']
            );
            array_push( $this->arrDistrict,$districtActivity);
        }
    }

    public function getDivisionArray()
    {
        $sql = 'SELECT id, name, prov_id, extent FROM division where prov_id = '.$this->provinceId. ' order by name;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $divisionActivity = array
            (
                'id' => $row['id'],
                'name' => $row['name'],
                'prov_id' => $row['prov_id'],
                'extent' => $row['extent']
            );
            array_push( $this->arrDivision,$divisionActivity);
        }
    }

    public function getProvinceArray()
    {
        $sql = 'SELECT id, name, extent FROM province where id = '.$this->provinceId. ' order by name;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $provinceActivity = array
            (
                'id' => $row['id'],
                'name' => $row['name'],
                'extent' => $row['extent']
            );
            array_push( $this->arrProvince,$provinceActivity);
        }
    }

    public function getJSONArray()
    {
        $this->getRevenueNames();
        $this->getMozaArray();
        $this->getTehsilArray();
        $this->getDistrictArray();
        $this->getDivisionArray();
        $this->getProvinceArray();

        $finalOutput = new stdClass();

        $finalOutput->namesArray = $this->arrRevenueNames;
        $finalOutput->mozaArray = $this->arrMoza;
        $finalOutput->tehsilArray = $this->arrTehsil;
        $finalOutput->districtArray = $this->arrDistrict;
        $finalOutput->divisionArray = $this->arrDivision;
        $finalOutput->provinceArray = $this->arrProvince;

        echo json_encode($finalOutput, JSON_NUMERIC_CHECK);
    }
}

$obj = new GetRevenueArrays();
echo $obj->getJSONArray();
$obj->closeConnection();
<?php
include ('./connection.php');

class Rdivision extends Connection{
    public function Rdivision(){
        $this->connectionDB();
    }
    public function  getJsonData()
    {
    if($_REQUEST['levelid']=='startload'){
        $lp=$_REQUEST['lp'];
        $sql="select(select extent from landplan where lp_sheet='$lp') as extent,(select uid from railway_div  where id in (select railway_div_id from section  where sec_code in (SELECT sec_code from landplan  where lp_sheet='$lp')))as uid,(SELECT sec_code from landplan  where lp_sheet='46-5-MUL')as id";
      //  echo $sql;
        $query = pg_query($sql);
        $row = pg_fetch_all($query);
        return json_encode($row);
    }elseif($_REQUEST['levelid']=='totalStats'){
        $sql="select (select count(*)  from railway_div)as totalDivision,(select count(*) from section)as totalSection,(select count(*) from landplan)as totalLandplans,(select count(*) from yardplan)as totalYardPlans,(select count(*) from siding)as totalSidings";
        $query = pg_query($sql);
        $row = pg_fetch_all($query);
        return json_encode($row);
    }elseif($_REQUEST['levelid']=='rdivision'){
            $divid=$_REQUEST['divid'];
            $sql = "WITH div_id as (SELECT distinct '" . $_REQUEST['divid'] . "'::text as id FROM railway_div)
          select c.* from (
	      select id,
            (SELECT count(s.sec_code) FROM section s WHERE s.railway_div_id =(select id from div_id)) sec_count,
            (SELECT count(m.name) FROM moza m, section s WHERE m.sec_code = s.sec_code AND s.railway_div_id = (select id from div_id)) moza_count,
            (select count (lp.lp_sheet) as lp_count from landplan lp,section s WHERE lp.sec_code = s.sec_code AND s.railway_div_id = (select id from div_id)) lp_count,
            (select count(sp.*) as parcel_count from survey_parcel sp, section s WHERE sp.section_id = s.sec_code AND s.railway_div_id = (select id from div_id)) parcel_count,
            (select round(sum(li.mh_sqyd)) as mh from landinfo li,railway_div rd where li.division = rd.name AND rd.id = (select id from div_id)) mh,
            (select round(sum(li.rh_sqyd)) as rh from landinfo li,railway_div rd where li.division = rd.name AND rd.id = (select id from div_id)) rh,
            (select round(sum(li.lp_sqyd)) as rh from landinfo li,railway_div rd where li.division = rd.name AND rd.id = (select id from div_id)) lp_area,
            (select count(*) from yardplan_lp where sec_code like '%$divid') yardplan,
            (select count(*) from siding where sec_code like '%$divid') siding
            from railway_div where id= (select id from div_id)
        ) c;";
         //   echo $sql;
            $query = pg_query($sql);
            $row = pg_fetch_all($query);
          //   $this->closeConnection();
            return json_encode($row);

        }elseif($_REQUEST['levelid']=='section'){
            $secid=$_REQUEST['secid'];
            $sql = "WITH sec_id as (SELECT distinct '" . $_REQUEST['secid'] . "'::text as id FROM section)
                    select c.* from (
                        select sec_code,
                        (SELECT count(m.name) FROM moza m, section s WHERE m.sec_code = s.sec_code AND s.sec_code = (select id from sec_id)) moza_count,
                        (select count (lp.lp_sheet) as lp_count from landplan lp,section s WHERE lp.sec_code = s.sec_code AND s.sec_code = (select id from sec_id)) lp_count,
                        (select count(sp.*) as parcel_count from survey_parcel sp WHERE sp.section_id = (select id from sec_id)) parcel_count,
                        (select round(sum(li.mh_sqyd)) as mh from landinfo li where li.lp_sheet like '%'||(select id from sec_id)::text) mh,
                        (select round(sum(li.rh_sqyd)) as rh from landinfo li where li.lp_sheet like '%'||(select id from sec_id)) rh,
                        (select round(sum(li.lp_sqyd)) as rh from landinfo li where li.lp_sheet like '%'||(select id from sec_id)) lp_area,
                        (select count(*) from yardplan_lp where sec_code='$secid') yardplan,
                        (select count(*) from siding where sec_code='$secid') siding
                        from section where sec_code= (select id from sec_id)
                    ) c;";
            $query = pg_query($sql);
            $row = pg_fetch_all($query);
            return json_encode($row);
        }elseif($_REQUEST['levelid']=='landplan'){

            $sql = $sql = "WITH lp_sheet as (SELECT distinct '" . $_REQUEST['lp'] . "'::text as id FROM section)
                select c.* from (
                    select lp_sheet,
                    (SELECT count(m.name) FROM moza m, section s WHERE m.sec_code = s.sec_code AND s.sec_code = (select id from lp_sheet)) moza_count,
                    (select count (ch.*) as ch_count from lp_chainage ch WHERE ch.lp_sheet = (select id from lp_sheet)) ch_count,
                    (select count(sp.*) as parcel_count from survey_parcel sp WHERE sp.lp_sheet = (select id from lp_sheet)) parcel_count,
                    (select round(sum(li.mh_sqyd)) as mh from landinfo li where li.lp_sheet like ('%". $_REQUEST['lp'] ."%')::text) mh,
                    (select round(sum(li.rh_sqyd)) as rh from landinfo li where li.lp_sheet like ('%". $_REQUEST['lp'] ."%')) rh,
                    (select round(sum(li.lp_sqyd)) as rh from landinfo li where li.lp_sheet like ('%". $_REQUEST['lp'] ."%')) lp_area,
                    (select moza from landplan where lp_sheet=(select id from lp_sheet)) moza,
                    (select min(mileage) from lp_mileage where lp_sheet='" . $_REQUEST['lp'] . "')t,
                    (select max(mileage) from lp_mileage where lp_sheet='" . $_REQUEST['lp'] . "') f
                    from landplan where lp_sheet= (select id from lp_sheet)
                ) c;";

       // echo $sql;
            $query = pg_query($sql);
            $row = pg_fetch_all($query);
            return json_encode($row);
        }elseif($_REQUEST['levelid']=='pro_stat'){
             $sql="select (select count(*) from division  where prov_id=".$_REQUEST['pro_id'].")as div_count,
                   (select count(*) from district  where div_id in (select id from division where prov_id=".$_REQUEST['pro_id'].")) as dist_count,
                   (select count(*) from tehsil where distt_id  in (select id from district where div_id in (select id from division where prov_id=".$_REQUEST['pro_id']."))) as teh_count,
                   (select count(*) from moza where tehsil_id in (select id from tehsil where distt_id in (select id from district where div_id in (select id from division where prov_id=".$_REQUEST['pro_id']."))))  as moza_count";
                $query = pg_query($sql);
                $row = pg_fetch_all($query);
                return json_encode($row);
         }elseif($_REQUEST['levelid']=='revStat'){
            $sql="select (select count(*) from province ) as pro,
                    (select count(*) from division ) as div,
                    (select count(*) from district) as distt,
                    (select count(*) from tehsil) as teh,
                    (select count(*) from moza) as moza,
                     (select count(*) from railway_div) as rwy_div,
                    (select count(*) from  section) as sec,
                    (select count(*) from landplan) as landplan,
                    (select count(*) from yardplan) as yardplan,
                    (select count(*) from siding) as siding,
                    (select count(*) from survey_parcel) as parcel";
            $query = pg_query($sql);
            $row = pg_fetch_all($query);
            return json_encode($row);
          }
        elseif($_REQUEST['levelid']=='parcel'){

            if($_REQUEST['survey_typ']=='Service Building'){
                $pin=$_REQUEST['pin'];
                $sub_parcel=$_REQUEST['sub_parcel'];

                $sql ="select * from tbl_officail_building WHERE unique_land_parcel_no='$pin' and parcel_sub_division='$sub_parcel'";

                $query = pg_query($sql);
                $row = pg_fetch_all($query);
                return json_encode($row);
            }elseif($_REQUEST['survey_typ']=='Government Department'){
                $pin=$_REQUEST['pin'];
                $sub_parcel=$_REQUEST['sub_parcel'];

                $sql ="select * from tbl_govt_other_department WHERE unique_land_parcel_no='$pin' and parcel_sub_division='$sub_parcel'";

                $query = pg_query($sql);
                $row = pg_fetch_all($query);
                return json_encode($row);

            }elseif($_REQUEST['survey_typ']=='Encroachment'){

                $pin=$_REQUEST['pin'];
                $sub_parcel=$_REQUEST['sub_parcel'];

                $sql ="select * from tbl_encroachment WHERE unique_land_parcel_no='$pin' and parcel_sub_division='$sub_parcel'";

                $query = pg_query($sql);
                $row = pg_fetch_all($query);
                return json_encode($row);

            }elseif($_REQUEST['survey_typ']=='Katchi Abadi'){

                $pin=$_REQUEST['pin'];
                $sub_parcel=$_REQUEST['sub_parcel'];

                $sql ="select * from tbl_katchiabadi WHERE unique_land_parcel_no='$pin' and parcel_sub_division='$sub_parcel'";

                $query = pg_query($sql);
                $row = pg_fetch_all($query);
                return json_encode($row);

            }elseif($_REQUEST['survey_typ']=='Leased Area For Shops'){

                $pin=$_REQUEST['pin'];
                $sub_parcel=$_REQUEST['sub_parcel'];

                $sql ="select * from tbl_leased_shop WHERE unique_land_parcel_no='$pin' and parcel_sub_division='$sub_parcel'";

                $query = pg_query($sql);
                $row = pg_fetch_all($query);
                return json_encode($row);

            }elseif($_REQUEST['survey_typ']=='Leased Area (for other than shops)'){

                $pin=$_REQUEST['pin'];
                $sub_parcel=$_REQUEST['sub_parcel'];

                $sql ="select * from tbl_leasedform_other WHERE unique_land_parcel_no='$pin' and parcel_sub_division='$sub_parcel'";
                $query = pg_query($sql);
                $row = pg_fetch_all($query);
                return json_encode($row);

            }elseif($_REQUEST['survey_typ']=='Open Land'){

                $pin=$_REQUEST['pin'];
                $sub_parcel=$_REQUEST['sub_parcel'];

                $sql ="select * from survey_parcel WHERE old_pin='$pin' and sub_parcel='$sub_parcel'";
                $query = pg_query($sql);
                $row = pg_fetch_all($query);
                return json_encode($row);

            }


    }
     //  echo  $row[0]['id'];
       // print_r($row);

//        $shops= [];
//        for ($i = 0; $i < count($row); $i++) {
//            $shop = [];
//            $shop['id'] = $row[$i]['id'];
//
//            $shop["uid"] = $row[$i]['uid'];
//
//            $shop["name"] = $row[$i]['name'];
//
//            $shops[] = $shop;
//
//
//        }
//        return $shops;


    }
}
$obj = new Rdivision();
echo $obj->getJsonData();

$obj->closeConnection();
<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/3/2015
 * Time: 5:04 PM
 */
include ('./connection.php');
class AreaCalculation extends Connection {
    public $mauzaName;
    public $foldMauzaName;
    function getAreaCalculation(){
        $mauza = $_GET['MAUZA'];
        $this->foldMauzaName =$mauza;

        $result =false;
        $con =  $this->connectionDB();
//        $sql = "Select li.moza mz, levenshtein(upper(Trim(li.moza)),upper(Trim('$mauza'))) distance  from landinfo li ORDER BY distance limit 1";
        $sql = "Select li.moza mz from landinfo li where upper(moza)=upper('".$mauza."')";
//        echo $sql."<br/><br/>";
        $query = pg_query($sql);
        $rows =pg_num_rows($query);
//        echo $query."  ".$rows."<br/><br/>";
        if($row = pg_fetch_object($query)) {
            $this->mauzaName = $row->mz;
            $sql = "SELECT round(((sum(railway_kanal)*20+sum(railway_marla))*272)/9,2) as \"Landplan\", round(((sum(l.mh_kanal)*20+ sum(l.mh_marla))*272)/9,2) as \"ROR Permanent\",
            round(((sum(l.rh_kanal)*20+sum(l.rh_marla))*272)/9,2) as \"ROR Periodical\" ,l.mh_owner \"Ownership MH\", l.rh_owner \"Ownership RH\"
            from landinfo l
            GROUP BY l.moza ,l.mh_owner , l.rh_owner
            having l.moza = '$this->mauzaName'";
//			echo "<br/>".$sql."<br/><br/>";
            $query = pg_query($sql);
            $result = pg_fetch_all($query);
        }
        $this->closeConnection();
        return $result;
    }
    function getImagesPath($recordType){
        try{
            $filenameArray = [];
            $folder =$this->foldMauzaName;// $this->mauzaName; //$_GET['MAUZA'];
            $rootPath =$_SERVER['DOCUMENT_ROOT'];
            $filesPath = '/Revenue Record/'.$folder.'/'.$recordType;

          //  $directory = dirname(realpath(__FILE__)).$filesPath;         //"/Revenue Record/".$folder."/".$recordType;
            $directory = $rootPath.'/Revenue Record/'.$folder.'/'.$recordType;
//            echo "<br/>".$directory;
            if( file_exists($directory)) {
                $handle = opendir($directory);
                if ($handle) {
                    while ($file = readdir($handle)) {
                        if ($file !== '.' && $file !== '..') {
                            array_push($filenameArray, "$filesPath/$file");
                        }
                    }
                }
            }
			//$this->closeConnection();
            return $filenameArray;
        }
        catch(Exception $ex){
            throw new Exception($ex->getMessage());
        }
    }
}
$cal = new AreaCalculation();
$result = $cal->getAreaCalculation();
//$imagesPathMH = $cal->getImagesPath("MH");
$imagesPathMH = $cal->getImagesPath("ROR Permanent");
//$imagesPathRH = $cal->getImagesPath("RH");
$imagesPathRH = $cal->getImagesPath("ROR Periodic");
$finalResult = array("areaCalculation"=>$result,"MHImages"=>$imagesPathMH,"RHImages"=>$imagesPathRH);
echo json_encode($finalResult);


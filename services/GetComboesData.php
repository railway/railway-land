<?php
/**
 * Created by PhpStorm.
 * User: idrees
 * Date: 10/1/2015
 * Time: 3:45 AM
 */
include ('./connection.php');

class GetComboesData extends Connection{
    public function GetComboesData(){
        $this->connectionDB();
    }
    public function  getJsonData()
    {
        $adminLevel = $_REQUEST['ADMIN_LEVEL'];
        $id = $_REQUEST['LEVEL_ID'];
        $sql = '';
        if ($adminLevel == 'province') {
            $sql = "select id, name, extent from province ORDER BY name";
        } else if ($adminLevel == 'division') {
            $sql = "select id, name, extent from division where prov_id = '$id' ORDER BY name";
        } else if ($adminLevel == 'district') {
            $sql = "select id, name, extent from district where div_id = '$id'  ORDER BY name";
        } else if ($adminLevel == 'tehsil') {
            $sql = "select id, name, extent from tehsil  where distt_id = '$id'  ORDER BY name";
        } else if ($adminLevel == 'moza') {
            $sql = "select id, name,sec_code, tehsil_id, extent from moza  where tehsil_id = '$id'  ORDER BY name";
        }else if ($adminLevel == 'mozalandplan') {
            $sql = "SELECT lp.lp_sheet,tehsil_id, lp.sec_code, lp.extent from landplan lp where lp.moza = '$id'";
        }
        else if ($adminLevel == 'rdivision') {
            $sql = "select id,uid, name, extent from railway_div ORDER BY name ASC";
        } else if ($adminLevel == 'section') {
            $sql = "SELECT s.name, s.sec_code as id, s.extent from section s where s.railway_div_id = '$id' ORDER BY s.name ASC";
        }else if ($adminLevel == 'landplan') {
            $sql="SELECT lp.lp_sheet,tehsil_id, lp.sec_code, lp.extent from landplan lp where lp.sec_code = '$id' ORDER BY lp.lp_sheet";
//            $sql="select * from yardplan_lp where sec_code='23-LHR'";
//            $sql="SELECT  * from siding  where sec_code='23-LHR'";
        }else if ($adminLevel == 'parcel') {
            $sql = "SELECT gid,unique_id,old_pin,survey_typ,sub_parcel , ST_XMax(geom) AS xmax, ST_XMin(geom) AS xmin, ST_YMax(geom) AS ymax, ST_YMin(geom) AS ymin  from survey_parcel  where lp_sheet = '$id'";
        }else if ($adminLevel == 'yardplan_lp') {
            $sql = "SELECT lp_sheet,yp_sheet,moza,tehsil_id from yardplan_lp where sec_code='$id'";
        }else if ($adminLevel == 'yardplan') {
            $sql = "SELECT extent,mileage,station from yardplan where yp_sheet='$id'";
        }else if ($adminLevel == 'siding') {
            $sql = "SELECT extent,moza,station,lp_sheet from siding where sec_code='$id'";
        }else if ($adminLevel == 'ypparcel') {
            $sql = "SELECT extent,moza,station,lp_sheet from siding where sec_code='$id'";
        }else if ($adminLevel == 'siding_parcel') {
            $sql = "SELECT id,pin,extent,moza,station,landuse from siding_parcel where lp_sheet='$id'";
        }else if ($adminLevel == 'chainage') {
            $sql = "SELECT id,ch_val,symbology,ST_XMax(geom) AS xmax, ST_XMin(geom) AS xmin, ST_YMax(geom) AS ymax, ST_YMin(geom) AS ymin from lp_chainage where lp_sheet='$id'";
        }else if ($adminLevel == 'mileage') {
            $division=$_REQUEST['division'];
            $sql = "select distinct(mileage) from lp_mileage where lp_sheet like '%$division'";
        }else if ($adminLevel == 'mileage_landplan') {
            $div=$_REQUEST['div'];
            $sql ="SELECT lp.lp_sheet,tehsil_id, lp.sec_code, lp.extent from landplan lp where lp.lp_sheet in (select distinct lp_sheet from lp_mileage where mileage::double precision >=".$_REQUEST['from']." and mileage::double precision <=".$_REQUEST['to']." and lp_sheet like '%$div')";
        }else if($adminLevel=='khasra'){
                 $moza=$_REQUEST['moza'];
              $sql="SELECT * from lp_khasra where moza='$moza'";
        }
//        else if ($adminLevel == 'yardplan') {
//            $sql = "SELECT yp.lp_sheet, yp.sec_code from yardplan_lp yp where yp.sec_code = '$id'";
//        }else if ($adminLevel == 'siding') {
//            $sql = "SELECT si.lp_sheet, si.sec_code, si.extent from siding si where si.sec_code= '$id'";
//        }
//        echo $sql;
        $query = pg_query($sql);
        $row = pg_fetch_all($query);
        if ($row) {
            json_encode($row);
            return json_encode($row);
        } else {
            echo "error";
            return false;
        }
    }
}
$obj = new GetComboesData();
echo $obj->getJsonData();
$obj->closeConnection();
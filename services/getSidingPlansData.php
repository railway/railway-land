<?php
include('./connection.php');
/**
 * Created by PhpStorm.
 * User: idrees
 * Date: 10/13/2015
 * Time: 11:52 PM
 */

class getLandPlansData extends connection{

    public $mauzaName = "";
    public $sheetNo = "";
    public $info = "";
    public $sec_code="";
    public  function getLandPlansData()
    {
    }

    public function getLandPlansDetail()
    {
        $resultFinal = "";
        try
        {
            $this->mauzaName = $_REQUEST["MAUZA"];
            $this->sheetNo = $_REQUEST["SHEET_NO"];
            $this->info = $_REQUEST["INFO"];
            $this->sec_code = $_REQUEST["SEC_CODE"];
            $con =  $this->connectionDB();
            $resultFinal = false;
            $sql = "";
            if($this->info == "Mileage")
            {
                $sql = "SELECT min(CAST(coalesce(replace(mileage , 'm ',''), '0') AS double precision)) fromMileage , max(CAST(coalesce(replace(mileage , 'm ',''), '0') AS double precision)) toMileage from siding_mileage where lp_sheet ='" . $this->sheetNo . "'";
            }
            else if ($this->info == "Chainage")
            {
                $sql = "select st_xmin(geom)||','||st_ymin(geom)||','||st_xmax(geom)||','||st_ymax(geom) as extent, \"ch_val\" \"Chainage\", feature \"Name\" from siding_chainage where lp_sheet ='" . $this->sheetNo . "'";
            }
            else if ($this->info == "Khasra")
            {
                $sql = "select extent, khasra_no \"Khasra No\"  from siding_khasra WHERE moza ='" . $this->mauzaName . "' and sec_code='".$this->sec_code."'";
            }
            $result = pg_query($sql);
            $resultFinal = pg_fetch_all($result);
        }
        catch (Exception $ex)
        {
            throw new Exception("401 : " . $ex->getTraceAsString());
        }
        $this->closeConnection();
        return $resultFinal;
    }
}

try
{
    $obj = new getLandPlansData();
    $output = $obj->getLandPlansDetail();
    echo json_encode($output);
}
catch (Exception $ex)
{
    echo $ex->getMessage();
}

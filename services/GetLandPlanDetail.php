<?php
include ('./connection.php');
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/2/2015
 * Time: 12:08 AM
 */

class GetMauzaDetail extends connection {
    public function GetMauzaDetail(){

    }
    public $mouzaName = null;
    public $infoRequired = null;
    public function urlParameters()
    {
        try {
            $this->mouzaName = $_REQUEST["MAUZA"];
            $this->sheetNo = $_REQUEST["SHEET_NO"];
            $this->infoRequired = $_REQUEST["INFO"];
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    public function getMauzaDetailFromDB(){
        $finalResult="";
        try {
            $this->urlParameters();
            $finalResult=false;
            $con =  $this->connectionDB();
            $sqlMauzaName="";
             if ($this->infoRequired == "Khasra") {
                $sqlMauzaName = "Select li.moza mz, levenshtein(upper(Trim(li.moza)),upper(Trim('$this->mouzaName'))) distance  from lp_khasra li ORDER BY distance limit 1";
                 $query = pg_query($sqlMauzaName);
                 if($row = pg_fetch_object($query)) {
                     $this->mouzaName = $row->mz;
                 }
            }
                $sql = "";
                if ($this->infoRequired == "Mileage") {
                    $sql = "SELECT min(CAST(coalesce(replace(\"mileage\" , 'm ',''), '0') AS double precision)) fromMileage , max(CAST(coalesce(replace(\"mileage\" , 'm ',''), '0') AS double precision)) toMileage from lp_mileage where lp_sheet ='" . $this->sheetNo . "'";
                } else if ($this->infoRequired == "Chainage") {
                    $sql = "select st_xmin(geom)||','||st_ymin(geom)||','||st_xmax(geom)||','||st_ymax(geom) as extent,\"id\" \"Chainage Id\", \"ch_val\" \"Chainage\", feature \"Name\" from lp_chainage where lp_sheet ='" . $this->sheetNo . "'";
                } else if ($this->infoRequired == "Khasra") {
                    $sql = "select st_xmin(geom)||','||st_ymin(geom)||','||st_xmax(geom)||','||st_ymax(geom) as extent, khasra_no \"Khasra No\"  from lp_khasra WHERE moza ='" . $this->mouzaName . "'";
                }
            //    echo $sql;
                $result = pg_query($sql); //or die('Query failed: ' . pg_last_error());
                $finalResult = pg_fetch_all($result);
      //      }
        //    $finalResult = array("milage"=>$milageArray,"chainage"=>$chainageArray);
        } catch (Exception $exc) {
            throw new Exception("401 : " . $exc->getTraceAsString());
        }

        $this->closeConnection();
        return $finalResult;
    }

}
try {
    $obj = new GetMauzaDetail();
    $output = $obj->getMauzaDetailFromDB();
    echo json_encode($output);
} catch (Exception $ex) {
    echo $ex->getMessage();
}
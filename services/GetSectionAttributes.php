<?php
include("./connection.php");
/**
 * Created by JetBrains PhpStorm.
 * User: User
 * Date: 5/29/15
 * Time: 12:17 PM
 * To change this template use File | Settings | File Templates.
 */
class GetSectionAttributes extends connection
{
    public $sectionName = null;
    public $infoRequired = null;
    public $adminLevel =null;
    public $mozaName=null;

    public function urlParameters()
    {
        try {
            $this->sectionName = $_REQUEST["SECTION"];
            $this->infoRequired = $_REQUEST["INFO"];
            $this->adminLevel = $_REQUEST['ADMIN_LEVEL'];
            $this->mozaName = $_REQUEST['MOZA'];
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    public function getSectionAttributesFromDB(){
        $out=false;
        try {
            $this->urlParameters();
            $colName='';
            $colVal='';
            if($this->adminLevel == 'Section'){
                $colName='sec_code';
                $colVal=$this->sectionName;
            }
            else if($this->adminLevel == 'Moza'){
                $colName='moza';
                $colVal=$this->mozaName;
            }
            $con =  $this->connectionDB();
            $sql="";
            if ($this->infoRequired == "LandPlan") {
                $sql = "select extent , lp_sheet \"LP Sheet\", moza \"Mauza\", sec_code \"rSection\", tehsil_id, station \"Station\" from landplan WHERE $colName = '$colVal'" ;
            }
            else if ($this->infoRequired == "YardPlan") {
                $sql = "select y.extent, y.yp_sheet \"YP Sheet\", y.mileage, l.moza \"Mauza\", l.sec_code \"rSection\", l.tehsil_id from yardplan y join yardplan_lp l on y.yp_sheet = l.yp_sheet where l.sec_code = '".$colVal."';" ;
            }
//            else if ($this->infoRequired == "YardPlan") {
//                $sql = "select extent, yp_sheet \"YP Sheet\",lp_sheet \"LP Sheet\", mileage, moza \"Mauza\", sec_code \"rSection\", tehsil_id from yardplan WHERE $colName = '$colVal'" ;
//            }
//            else if ($this->infoRequired == "AdditionalLand") {
//                $sql = "select extent,  nearest_landplan, nearest_landplan_sheet_no, district \"district\" ,railway_div \"rDivision\", section \"rSection\", tehsil_id from additional_land WHERE $colName = '$colVal'" ;
//            }
            else if ($this->infoRequired == "SidingPlan") {
                $sql = "select extent, lp_sheet \"LP Sheet\",  moza \"Mauza\", tehsil_id, sec_code \"rSection\" from siding WHERE sec_code = '".$colVal."';" ;
            }
//            else if ($this->infoRequired == "Station") {
//                $sql = "select extent, fea_name \"Railway Station\" from r_chainage where fea_name = 'Station Building' and sec_code ='".$this->sectionName."'" ;
//            }

            else if ($this->infoRequired == "areaDivision") {
                $sql = "select extent , lp_sheet \"LP Sheet\", moza \"Mauza\", sec_code \"rSection\", tehsil_id, station \"Station\" from landplan WHERE $colName = '$colVal'" ;
            }
            else if ($this->infoRequired == "areaSection") {
                $sql = "select extent , lp_sheet \"LP Sheet\", moza \"Mauza\", sec_code \"rSection\", tehsil_id, station \"Station\" from landplan WHERE $colName = '$colVal'" ;
            }
            else if ($this->infoRequired == "areaLandplan") {
                $sql = "select extent , lp_sheet \"LP Sheet\", moza \"Mauza\", sec_code \"rSection\", tehsil_id, station \"Station\" from landplan WHERE $colName = '$colVal'" ;
            }

               $result = pg_query($sql) or die('Query failed: ' . pg_last_error());
            if ($result) {
                $out = pg_fetch_all($result);
            }
            else{
                $out = [];
            }
        } catch (Exception $exc) {
            throw new Exception("401 : " . $exc->getTraceAsString());
        }
        $this->closeConnection();
        return $out;
    }
}
try {
    $obj = new GetSectionAttributes();
    $output = $obj->getSectionAttributesFromDB();
    echo stripslashes(json_encode($output));
} catch (Exception $ex) {
    echo $ex->getMessage();
}
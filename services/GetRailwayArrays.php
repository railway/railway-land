<?php
/**
 * Created by PhpStorm.
 * User: idrees
 * Date: 10/17/2015
 * Time: 9:17 AM
 */
include ('./connection.php');
class GetRailwayArrays extends Connection{

    public $arrSection = array();
    public $arrDivision = array();
    public $arrRailwayNames = array();

    public $sectionId = null;
    public $divisionId = null;

    public $sectionName = '';
    public $divisionName = '';

    public function GetRailwayArrays()
    {
        $this->connectionDB();
        $this->getRailwayIds();
    }

    public function getRailwayIds()
    {
        $mozaName = $_REQUEST['MOZA_ID'];
        $tehsilId = $_REQUEST['TEHSIL_ID'];

        $sql = 'select getReverseRailwayIds(\''.$mozaName.'\', '.$tehsilId.') as id_list;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $Id_Lists = $row['id_list'];
            $splittedIds = explode(",", $Id_Lists);

            $this->sectionId = $splittedIds[0];
            $this->divisionId = $splittedIds[1];

            $this->sectionName = $splittedIds[2];
            $this->divisionName = $splittedIds[3];
        }
    }

    public  function getRailwayNames()
    {
        $nameActivity = array
        (
            'division' => $this->divisionName,
            'section' => $this->sectionName
        );
        array_push($this->arrRailwayNames, $nameActivity);
    }
    public function getSectionArray()
    {
        $sql = 'SELECT sec_code as id, name, extent FROM section where rwy_div = \''.$this->divisionId. '\' order by name;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $sectionActivity = array
            (
                'id' => $row['id'],
                'name' => $row['name'],
                'extent' => $row['extent']
            );
            array_push( $this->arrSection,$sectionActivity);
        }
    }
    public function getDivisionArray()
    {
        $sql = 'SELECT id, name, extent FROM railway_div where id = \''.$this->divisionId. '\'order by name;';
        $query = pg_query($sql);
        while ($row = pg_fetch_array($query))
        {
            $divisionActivity = array
            (
                'id' => $row['id'],
                'name' => $row['name'],
                'extent' => $row['extent']
            );
            array_push( $this->arrDivision,$divisionActivity);
        }
    }
    public function getJSONArray()
    {
        $this->getRailwayNames();
        $this->getSectionArray();
        $this->getDivisionArray();

        $finalOutput = new stdClass();

        $finalOutput->namesArray = $this->arrRailwayNames;
        $finalOutput->sectionArray = $this->arrSection;
        $finalOutput->divisionArray = $this->arrDivision;

        echo json_encode($finalOutput, JSON_NUMERIC_CHECK);
    }
}


$obj = new GetRailwayArrays();
echo $obj->getJSONArray();
$obj->closeConnection();
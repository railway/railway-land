<?php
require("../fpdf/fpdf.php");
include ('./connection.php');
class PDF extends FPDF
{

function Header()
{
// Logo
    if($this->page==1) {
        $this->Image('../images/Railway-Land-Banner-1.png',10,6,190);
// Arial bold 15
        $this->SetFont('Arial', 'B', 15);
// Move to the right
      //  $this->Cell(60);
// Title
     //   $this->Cell(80, 10, 'Railway Land Record System', 1, 0, 'C');
// Line break
//$this->Ln(20);
        //$this->Ln(15, $this->y, 200, $this->y);
    }
}

// Page footer
function Footer()
{
// Position at 1.5 cm from bottom
$this->SetY(-15);
// Arial italic 8
$this->SetFont('Arial','I',8);
// Page number
$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
}



}

$con=new Connection();
$conn=$con->connectionDB();

$sql = "WITH lp_sheet as (SELECT distinct '" . $_REQUEST['lp'] . "'::text as id FROM section)
                select c.* from (
                    select lp_sheet,
                    (SELECT count(m.name) FROM moza m, section s WHERE m.sec_code = s.sec_code AND s.sec_code = (select id from lp_sheet)) moza_count,
                    (select count (ch.*) as ch_count from lp_chainage ch WHERE ch.lp_sheet = (select id from lp_sheet)) ch_count,
                    (select count(sp.*) as parcel_count from survey_parcel sp WHERE sp.lp_sheet = (select id from lp_sheet)) parcel_count,
                    (select round(sum(li.mh_sqyd)) as mh from landinfo li where li.lp_sheet =(select id from lp_sheet)::text) mh,
                    (select round(sum(li.rh_sqyd)) as rh from landinfo li where li.lp_sheet =(select id from lp_sheet)) rh,
                    (select round(sum(li.lp_sqyd)) as rh from landinfo li where li.lp_sheet =(select id from lp_sheet)) lp_area,
                    (select moza from landplan where lp_sheet=(select id from lp_sheet)) moza,
                    (select min(mileage) from lp_mileage where lp_sheet='" . $_REQUEST['lp'] . "')t,
                    (select max(mileage) from lp_mileage where lp_sheet='" . $_REQUEST['lp'] . "') f
                    from landplan where lp_sheet= (select id from lp_sheet)
                ) c;";
 //echo $sql;
$query = pg_query($sql);
$row = pg_fetch_all($query);
//print_r($row);
//echo $row[0]['lp_sheet'];
//exit();

// Instanciation of inherited class
$pdf = new PDF();
//$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetFont('Times','',8);
$pdf->Ln(10);
$pdf->Cell(50,10,'Railway hierarchy:',0,0);
$pdf->Ln(10);
$pdf->Cell(40,10,'Division',1,0,'C');
$pdf->Cell(80,10,'Section',1,0,'C');
$pdf->Cell(40,10,'Moza',1,0,'C');
$pdf->Ln(10);
$pdf->Cell(40,10,$_REQUEST['div'],1,0,'C');
$pdf->Cell(80,10,$_REQUEST['sec'],1,0,'C');
$pdf->Cell(40,10,$row[0]['moza'],1,0,'C');

$pdf->Ln(10);
$pdf->Cell(50,10,'Landplan Info:',0,0);
$pdf->Ln(10);
$pdf->Cell(20,10,'LandPlan',1,0,'C');
$pdf->Cell(20,10,'Total Chainage',1,0,'C');
$pdf->Cell(20,10,'Total Parcel',1,0,'C');
$pdf->Cell(20,10,'Mileage From',1,0,'C');
$pdf->Cell(20,10,'Mileage To',1,0,'C');
$pdf->Cell(20,10,'Landplan Area',1,0,'C');
$pdf->Cell(25,10,'ROR Permananent',1,0,'C');
$pdf->Cell(20,10,'ROR Perodic',1,0,'C');
$pdf->Cell(30,10,'Moza',1,0,'C');
//$pdf->Cell(40,10,$row[0]['lp_sheet'],1,0,'C');
$pdf->Ln(10);
$pdf->Cell(20,10,$row[0]['lp_sheet'],1,0,'C');
$pdf->Cell(20,10,$row[0]['ch_count'],1,0,'C');
$pdf->Cell(20,10,$row[0]['parcel_count'],1,0,'C');
$pdf->Cell(20,10,$row[0]['f'],1,0,'C');
$pdf->Cell(20,10,$row[0]['t'],1,0,'C');
$pdf->Cell(20,10,$row[0]['lp_area'],1,0,'C');
$pdf->Cell(25,10,$row[0]['mh'],1,0,'C');
$pdf->Cell(20,10,$row[0]['rh'],1,0,'C');
$pdf->Cell(30,10,$row[0]['moza'],1,0,'C');
$pdf->Ln(10);
$pdf->SetX(60);


$pdf->Ln(10);
$pdf->Cell(50,10,'Register Record:',0,0);

$pdf->Ln(10);
$pdf->SetX(10);
$pdf->Cell(95,10,'ROR Permanent',1,0,'C');
$pdf->Cell(95,10,'ROR Perodic',1,0,'C');
$pdf->Ln(10);
//$pdf->SetX(30);
//$pdf->Image('http://202.166.167.126/Revenue%20Record/CHAK%20NO.122%20G.B/ROR%20Permanent/IMG_0193.JPG');


if($_REQUEST['permanent']=="undefined" && $_REQUEST['priodic']=="undefined") {
    $pdf->Cell(95, 142,'No Image Found', 1, 0, 'C');

    $pdf->Cell(95, 142,'No image Found', 1, 0, 'C');

}else{
    $pdf->Ln(1);
    $pdf->Cell(95, 142, $pdf->Image('http://202.166.167.126/Revenue%20Record/'.rawurlencode($row[0]['moza']).'/ROR%20Permanent/'.$_REQUEST['permanent'], $pdf->GetX(), $pdf->GetY(), 94), 1, 0, 'C');

    $pdf->Cell(95, 142, $pdf->Image('http://202.166.167.126/Revenue%20Record/'.rawurlencode($row[0]['moza']).'/ROR%20Periodic/'.$_REQUEST['priodic'], $pdf->GetX(), $pdf->GetY(), 94), 1, 0, 'C');
}


//this is for landplan and mussavi


//$pdf->SetX(30);
//$pdf->Image('http://202.166.167.126/Revenue%20Record/CHAK%20NO.122%20G.B/ROR%20Permanent/IMG_0193.JPG');


//if(isset($_REQUEST['permanent'])) {
  // echo 'http://202.166.167.126/railway_raster/Railway/Lahore%20Division/Land%20Plan/Faisalabad-Jaranwala/'.rawurlencode($row[0]['moza']);
  //echo 'http://202.166.167.126/railway_raster/Railway/Lahore%20Division/Land%20Plan/'.$_REQUEST['sec'].'/'.rawurlencode($row[0]['moza']).'.img.jpg';
//$file=$pdf->Image('http://202.166.167.126/railway_raster/Railway/Lahore%20Division/Land%20Plan/'.rawurlencode($_REQUEST['sec']).'/'.rawurlencode($row[0]['moza']).'.img.jpg');
//if(file_exists($file)) {
//    $pdf->Ln(150);
//    $pdf->Cell(50,10,'Landplan and Mussavi:',0,0);
//
//    $pdf->Ln(10);
//    $pdf->SetX(10);
//    $pdf->Cell(95,10,'Landplan',1,0,'C');
//    $pdf->Cell(95,10,'Mussavi',1,0,'C');
//    $pdf->Ln(10);
//    $pdf->Ln(1);
//    $pdf->Cell(95, 142, $pdf->Image('http://202.166.167.126/railway_raster/Railway/Lahore%20Division/Land%20Plan/' . rawurlencode($_REQUEST['sec']) . '/' . rawurlencode($row[0]['moza']) . '.img.jpg', $pdf->GetX(), $pdf->GetY(), 94), 1, 0, 'C');
////}
////if(isset($_REQUEST['priodic'])) {
//    $pdf->Cell(95, 142, $pdf->Image('http://202.166.167.126/railway_raster/Railway/Lahore%20Division/Musavee/' . rawurlencode($_REQUEST['sec']) . '/' . rawurlencode($row[0]['moza']) . '.img.jpg', $pdf->GetX(), $pdf->GetY(), 94), 1, 0, 'C');
////}
//}


$pdf->Output();
?>
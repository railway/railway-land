<?php
/**
 * Created by PhpStorm.
 * User: Adeel Mahmood
 * Date: 7/1/2015
 * Time: 10:36 PM
 */


include("./connection.php");
class LandInfo_Mauza extends connection
{

    public function LandInfo_Mauza()
    {

    }
    function getLandInfo(){
        $mauza = $_GET['MAUZA'];
//$mauza=array();
        $actualSUM = array();
//$division_query="select * from landplan where upper(section_) =upper('".$section."')";
//$query = pg_query($division_query);
//while($row = pg_fetch_array($query))
//{
//    array_push($mauza,$row['mouza']);
//}
        $sumKanal = 0;
        $sumMarla = 0;
        $sumMHKanal = 0;
        $sumMHMarla = 0;
        $sumRHKanal = 0;
        $sumRHMarla = 0;
        $con =  $this->connectionDB();
//for($i=0;$i<sizeof($mauza);$i++){
        $sql = "select sum(railway_kanal) as totalkanal,sum(railway_marla) as totalmarla,sum(mh_kanal) as totalmhkanal, sum(mh_marla) as totalmhmarla,sum(rh_kanal) as totalrhkanal, sum(rh_marla) as totalrhmarla from landinfo where upper(mauza)=upper('".$mauza."')";
//echo "<br/>".$sql;
        $query = pg_query($sql);
        $rows = pg_num_rows($query);
        while ($row = pg_fetch_array($query))
        {
            if ($row['totalkanal'])
            {
                $sumKanal = $sumKanal + intval($row['totalkanal']);
            }

            if ($row['totalmarla']) {
                $sumMarla = $sumMarla + intval($row['totalmarla']);
            }
            if ($row['totalmhkanal']) {
                $sumMHKanal = intval($sumMHKanal) + intval($row['totalmhkanal']);
            }
            if ($row['totalmhmarla']) {
                $sumMHMarla = intval($sumMHMarla) + intval($row['totalmhmarla']);
            }
            if ($row['totalrhkanal']) {
                $sumRHKanal = intval($sumRHKanal) + intval($row['totalrhkanal']);
            }
            if ($row['totalrhmarla']) {
                $sumRHMarla = intval($sumRHMarla) + intval($row['totalrhmarla']);
            }

        }
//}
        if ($sumMarla > 20) {

            if (intval($sumMarla) % 20 == 0) {
                $resMarla = intval($sumMarla) / 20;
                $sumKanal = intval($sumKanal) + intval($resMarla);
                $sumMarla = 0;
            } else {
                $resMarla = intval($sumMarla) / 20;
                $explode_array = explode(".", $resMarla);
                $sumKanal = intval($sumKanal) + $explode_array[0];
                // echo "a: ".$explode_array[1]." - ".substr($explode_array[1],0,-1);
                $m = intval(substr($explode_array[1], 0, -1)) * 2;
                $sumMarla = intval($m);


            }
        }
        if ($sumMHMarla > 20) {

            if (intval($sumMHMarla) % 20 == 0) {
                $resMarla = intval($sumMHMarla) / 20;
                $sumMHKanal = intval($sumMHKanal) + intval($resMarla);
                $sumMHMarla = 0;
            } else {
                $resMarla = intval($sumMHMarla) / 20;
                $explode_array = explode(".", $resMarla);
                $sumMHKanal = intval($sumMHKanal) + $explode_array[0];
                // echo "a: ".$explode_array[1]." - ".substr($explode_array[1],0,-1);
                $m = intval(substr($explode_array[1], 0, -1)) * 2;
                $sumMHMarla = intval($m);


            }

        }
        if ($sumRHMarla > 20) {

            if (intval($sumRHMarla) % 20 == 0) {
                $resMarla = intval($sumRHMarla) / 20;
                $sumRHKanal = intval($sumRHKanal) + intval($resMarla);
                $sumRHMarla = 0;
            } else {
                $resMarla = intval($sumRHMarla) / 20;
                $explode_array = explode(".", $resMarla);
                $sumRHKanal = intval($sumRHKanal) + $explode_array[0];
                // echo "a: ".$explode_array[1]." - ".substr($explode_array[1],0,-1);
                $m = intval(substr($explode_array[1], 0, -1)) * 2;
                $sumRHMarla = intval($m);

            }

        }

        $strArray = array();
        $valJson = new stdClass();
        $actualSumObj = new stdClass();
        $valJson->{'Railway Land'} = $sumKanal.' kanal '.$sumMarla.' Marla';
//        $valJson->{'Railway Land'} = $sumMarla.' Marla';
        $valJson->{'Record of Rights (Permanant)'} = $sumMHKanal.' Kanal'.$sumMHMarla.' Marla';
//        $valJson->{'Record of Rights (Permanant)'} = $sumMHMarla.' Marla';
        $valJson->{'Record of Rights (Periodic) '} = $sumRHKanal.' Kanal'.$sumRHMarla.' Marla';
//        $valJson->{'Record of Rights (Periodic) '} = $sumRHMarla.' Marla';
        $actualSumObj->ActualSUM = $valJson;

        array_push($strArray, $actualSumObj);

        $valJsonDiff = new stdClass();
        $diffSumObj = new stdClass();
        $valJsonDiff->LP_MH_Kanal = intval($sumKanal) - intval($sumMHKanal);
        $valJsonDiff->LP_MH_Marla = intval($sumMarla) - intval($sumMHMarla);
        $valJsonDiff->LP_RH_Kanal = intval($sumKanal) - intval($sumRHKanal);
        $valJsonDiff->LP_RH_Marla = intval($sumMarla) - intval($sumRHMarla);
        $valJsonDiff->MH_RH_Kanal = intval($sumMHKanal) - intval($sumRHKanal);
        $valJsonDiff->MH_RH_Marla = intval($sumMHMarla) - intval($sumRHMarla);
        $diffSumObj->DiffSUM = $valJsonDiff;

        $actualSumObj->DIFFSUM = $valJsonDiff;
        array_push($strArray, $diffSumObj);
// array_push($actualSUM,$valJson);
        return json_encode($actualSumObj);
    }

}
try {
    $obj = new LandInfo_Mauza();
    $output = $obj->getLandInfo();
    echo $output;
} catch (Exception $ex) {
    echo $ex->getMessage();
}

?>
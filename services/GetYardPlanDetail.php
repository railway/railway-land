<?php
include("./connection.php");
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/2/2015
 * Time: 12:08 AM
 */

class GetYardPlanDetail extends connection {
    public function GetYardPlanDetail(){

    }
    public $sheetNo = null;
    public $infoRequired = null;
    public function urlParameters()
    {
        try {
            $this->sheetNo = $_REQUEST["SHEET_NO"];
            $this->infoRequired = $_REQUEST["INFO"];
        } catch (Exception $ex) {
            throw new Exception($ex->getMessage());
        }
    }
    public function GetYardPlanDetailFromDB(){
        $finalResult="";
        try {
            $this->urlParameters();
            $finalResult=false;
            $con =  $this->connectionDB();
                $sql = "";
                if ($this->infoRequired == "Mileage") {
                    $sql = "SELECT min(CAST(coalesce(\"name\", '0') AS integer)) fromMileage , max(CAST(coalesce(\"name\", '0') AS integer)) toMileage from r_milage where mouza ='" . $this->mouzaName . "'";
                } else if ($this->infoRequired == "Chainage") {
                    $sql = "select st_xmin(geom)||','||st_ymin(geom)||','||st_xmax(geom)||','||st_ymax(geom) as extent, concat('CH-',\"ch_val\") \"Chainage\"    from yp_chainage where yp_sheet ='" . $this->sheetNo . "'";
                } else if ($this->infoRequired == "Parcel") {
                    $sql = "select extent, code_disc \"Description\" from yp_parcel where yp_sheet ='" . $this->sheetNo . "'";
                }
             //   echo $sql;
                $result = pg_query($sql); //or die('Query failed: ' . pg_last_error());
                $finalResult = pg_fetch_all($result);
//            }
            //    $finalResult = array("milage"=>$milageArray,"chainage"=>$chainageArray);
        } catch (Exception $exc) {
            throw new Exception("401 : " . $exc->getTraceAsString());
        }

        $this->closeConnection();
        return $finalResult;
    }

}
try {
    $obj = new GetYardPlanDetail();
    $output = $obj->GetYardPlanDetailFromDB();
    echo json_encode($output);
} catch (Exception $ex) {
    echo $ex->getMessage();
}
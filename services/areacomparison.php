<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 7/3/2015
 * Time: 5:04 PM
 */
include ('./connection.php');
class AreaCalculation extends Connection {
    public $division;
    public $section;
    function getAreaCalculation(){
        //$mauza = $_GET['MAUZA'];

        $result =false;       // $this->foldMauzaName =$mauza;
        $division=$_GET['DIVISION'];
        $section=$_GET['SECTION'];

        $con =  $this->connectionDB();
        if($division == 'null' && $section == 'null') {
            $sql = "SELECT round(((sum(railway_kanal)*20 + sum(railway_marla))*272)/9,2) as \"All\", round(((sum(l.mh_kanal)*20 + sum(l.mh_marla))*272)/9,2) as \"ROR Permanent\",
                round(((sum(l.rh_kanal)*20 + sum(l.rh_marla))*272)/9,2) as \"ROR Periodical\" from landinfo l GROUP BY l.division";
            $query = pg_query($sql);
            $result = pg_fetch_all($query);
            $this->closeConnection();
            return $result;
        }
        else if($division != 'null' && $section == 'null') {
            $sql = "SELECT round(((sum(railway_kanal)*20 + sum(railway_marla))*272)/9,2) as \"Division\", round(((sum(l.mh_kanal)*20 + sum(l.mh_marla))*272)/9,2) as \"ROR Permanent\",
                round(((sum(l.rh_kanal)*20 + sum(l.rh_marla))*272)/9,2) as \"ROR Periodical\" from landinfo l GROUP BY l.division having upper(l.division) = upper('" . $division . "');";
            $query = pg_query($sql);
            $result = pg_fetch_all($query);
            $this->closeConnection();
            return $result;
        }
        else if($division != 'null' && $section != 'null') {
            $sql = "SELECT round(((sum(railway_kanal)*20 + sum(railway_marla))*272)/9,2) as \"Section\", round(((sum(l.mh_kanal)*20 + sum(l.mh_marla))*272)/9,2) as \"ROR Permanent\",
            round(((sum(l.rh_kanal)*20 + sum(l.rh_marla))*272)/9,2) as \"ROR Periodical\" from landinfo l GROUP BY l.section, l.division having upper(l.division) = upper('" . $division . "') and upper(l.section) = upper('" . $section . "')";
            $query = pg_query($sql);
            $result = pg_fetch_all($query);
            $this->closeConnection();
            return $result;
        }
    }
}
$cal = new AreaCalculation();
$result = $cal->getAreaCalculation();

$finalResult = array("areaCalculation"=>$result);
echo json_encode($finalResult);

